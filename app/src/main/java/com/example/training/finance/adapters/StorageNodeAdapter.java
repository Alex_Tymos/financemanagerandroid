package com.example.training.finance.adapters;


import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.example.training.finance.activities.edit_activities.EditStorageActivity;
import com.example.training.finance.adapters.abstracts.TreeNodeListAdapter;
import com.example.training.finance.adapters.holders.StorageViewHolder;
import com.example.training.finance.core.storage.database.Initializer;
import com.example.training.finance.core.storage.exceptions.CurrencyException;
import com.example.training.finance.core.storage.impl.DefaultStorage;
import com.example.training.finance.core.storage.interfaces.Storage;
import com.example.training.finance.utils.AppContext;
import com.example.training.finance.utils.CurrencyUtils;

import java.math.BigDecimal;

public class StorageNodeAdapter extends TreeNodeListAdapter<Storage, StorageViewHolder> {

    private static final String TAG = StorageNodeAdapter.class.getName();

    public StorageNodeAdapter(int mode) {
        super(mode, Initializer.getStorageSynchronizer());
    }

    @Override
    public StorageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        super.onCreateViewHolder(parent, viewType);
        return new StorageViewHolder(itemView);
    }

    @Override
    protected void openActivityOnClick(Storage storage, int requestCode) {

        Storage s;

        switch (requestCode) {
            case AppContext.REQUEST_NODE_ADD_CHILD:
                s = new DefaultStorage();
                break;

            default:
                s = storage;
        }

        Intent intent = new Intent(activityContext, EditStorageActivity.class);
        intent.putExtra(AppContext.NODE_OBJECT, s);
        activityContext.startActivityForResult(intent, requestCode, ActivityOptionsCompat.makeSceneTransitionAnimation(activityContext).toBundle());
    }

    @Override
    public void onBindViewHolder(StorageViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        final Storage storage = adapterList.get(position);

        try {
            BigDecimal approxAmount = storage.getApproxAmount(CurrencyUtils.defaultCurrency);
            if (approxAmount != null && !approxAmount.equals(BigDecimal.ZERO)) {
                approxAmount = approxAmount.setScale(0, BigDecimal.ROUND_UP);
                holder.tvAmount.setText("~ " + String.valueOf(approxAmount) + " " + CurrencyUtils.defaultCurrency.getSymbol());

                if (holder.tvAmount.getVisibility() == View.INVISIBLE || holder.tvAmount.getVisibility() == View.GONE) {
                    holder.tvAmount.setVisibility(View.VISIBLE);
                }
            } else {
                holder.tvAmount.setVisibility(View.INVISIBLE);
            }

        } catch (CurrencyException e) {
            Log.e(TAG, e.getMessage());
        }
    }
}
