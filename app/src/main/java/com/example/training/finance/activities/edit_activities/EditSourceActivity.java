package com.example.training.finance.activities.edit_activities;


import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.training.finance.R;
import com.example.training.finance.activities.abstracts.BaseEditNodeActivity;
import com.example.training.finance.core.storage.enums.OperationType;
import com.example.training.finance.core.storage.interfaces.Source;
import com.example.training.finance.objects.LocalizedOperationType;
import com.example.training.finance.utils.AppContext;
import com.example.training.finance.utils.OperationTypeUtils;

import java.util.ArrayList;
import java.util.List;

public class EditSourceActivity extends BaseEditNodeActivity<Source> {

    private Spinner spSourceType;
    private ArrayAdapter<LocalizedOperationType> spAdapter;

    public EditSourceActivity() {
        super(R.layout.activity_edit_source);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        createTypesSpinner();

        imgSave.setOnClickListener(v -> {

            String newName = etName.getText().toString();

            if (newName.trim().length() == 0) {
                Toast.makeText(EditSourceActivity.this, R.string.enter_name, Toast.LENGTH_SHORT).show();
                return;
            }

            if (edited(node, newName, newIconName)) {

                node.setName(newName);
                node.setOperationType(((LocalizedOperationType) spSourceType.getSelectedItem()).getOperationType());

                if (newIconName != null) {
                    node.setIconName(newIconName);
                }

                Intent intent = new Intent();
                intent.putExtra(AppContext.NODE_OBJECT, node);// передаем отредактированный объект, который нужно сохранить в БД
                setResult(RESULT_OK, intent);
            }

            transition.finishWithTransition();
        });
    }

    private void createTypesSpinner() {
        spSourceType = findViewById(R.id.sp_source_type);

        List<LocalizedOperationType> listTypes = new ArrayList<>(2);
        listTypes.add(OperationTypeUtils.incomeType);
        listTypes.add(OperationTypeUtils.outcomeType);

        spAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, listTypes);
        spSourceType.setAdapter(spAdapter);

        if (node.getOperationType() != null) {
            spSourceType.setEnabled(false);
            spSourceType.setSelection(OperationType.getList().indexOf(node.getOperationType()));
        } else {
            spSourceType.setEnabled(true);
        }
    }

    protected boolean edited(Source node, String newName, String newIconName) {

        return (node.getName() == null && newName != null) ||
                (node.getIconName() == null && newIconName != null) ||
                (node.getName() != null && newName != null && !node.getName().equals(newName)) ||
                (node.getIconName() != null && newIconName != null && !node.getIconName().equals(newIconName));
    }
}
