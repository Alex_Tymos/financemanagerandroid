package com.example.training.finance.objects;


import com.example.training.finance.utils.LocaleUtils;

import java.util.Currency;

public class LocalizedCurrency {

    private Currency currency;

    public LocalizedCurrency(Currency currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return currency.getSymbol(LocaleUtils.defaultLocale);
    }

    public Currency getCurrency() {
        return currency;
    }
}
