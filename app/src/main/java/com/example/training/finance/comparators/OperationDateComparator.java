package com.example.training.finance.comparators;


import com.example.training.finance.core.storage.interfaces.Operation;

import java.util.Comparator;

public class OperationDateComparator implements Comparator<Operation> {

    private static OperationDateComparator instance;

    private OperationDateComparator() {
    }

    public static OperationDateComparator getInstance() {
        return instance == null ? instance = new OperationDateComparator() : instance;
    }

    @Override
    public int compare(Operation o1, Operation o2) {
        return o2.getDateTime().compareTo(o1.getDateTime());
    }

}

