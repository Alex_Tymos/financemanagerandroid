package com.example.training.finance.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.training.finance.adapters.abstracts.TreeNodeListAdapter;
import com.example.training.finance.core.storage.interfaces.TreeNode;
import com.example.training.finance.listeners.TreeNodeActionListener;


public class TreeNodeListFragment<T extends TreeNode> extends BaseNodeListFragment<T, TreeNodeListAdapter, TreeNodeActionListener> {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);
        return view;
    }

    public void insertChildNode(TreeNode node) {
        adapter.insertChildNode(node);
    }
}
