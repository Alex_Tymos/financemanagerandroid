package com.example.training.finance.adapters;


import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;

import com.example.training.finance.R;
import com.example.training.finance.activities.edit_activities.edit_operation.EditConvertOperationActivity;
import com.example.training.finance.activities.edit_activities.edit_operation.EditIncomeOperationActivity;
import com.example.training.finance.activities.edit_activities.edit_operation.EditOutcomeOperationActivity;
import com.example.training.finance.activities.edit_activities.edit_operation.EditTransferOperationActivity;
import com.example.training.finance.adapters.abstracts.BaseNodeListAdapter;
import com.example.training.finance.adapters.holders.OperationViewHolder;
import com.example.training.finance.comparators.OperationDateComparator;
import com.example.training.finance.core.storage.database.Initializer;
import com.example.training.finance.core.storage.impl.operations.ConvertOperation;
import com.example.training.finance.core.storage.impl.operations.IncomeOperation;
import com.example.training.finance.core.storage.impl.operations.OutcomeOperation;
import com.example.training.finance.core.storage.impl.operations.TransferOperation;
import com.example.training.finance.core.storage.interfaces.Operation;
import com.example.training.finance.listeners.BaseNodeActionListener;
import com.example.training.finance.utils.AppContext;
import com.example.training.finance.utils.ColorUtils;
import com.example.training.finance.utils.IconUtils;
import com.example.training.finance.utils.LocaleUtils;
import com.example.training.finance.utils.OperationTypeUtils;

import java.util.Calendar;

public class OperationListAdapter extends BaseNodeListAdapter<Operation, OperationViewHolder, BaseNodeActionListener<Operation>> {

    private static final String TAG = OperationListAdapter.class.getName();

    public OperationListAdapter() {
        super(Initializer.getOperationSynchronizer(), R.layout.operation_item);

        comparator = OperationDateComparator.getInstance();
    }

    @Override
    protected void openActivityOnClick(Operation node, int requestCode) {

        Operation operation = null;
        Class activityClass = null;

        switch (requestCode) {

            case AppContext.REQUEST_NODE_ADD:
                break;

            case AppContext.REQUEST_NODE_EDIT:

                switch (node.getOperationType()) {
                    case INCOME:
                        activityClass = EditIncomeOperationActivity.class;
                        break;
                    case OUTCOME:
                        activityClass = EditOutcomeOperationActivity.class;
                        break;
                    case TRANSFER:
                        activityClass = EditTransferOperationActivity.class;
                        break;
                    case CONVERT:
                        activityClass = EditConvertOperationActivity.class;
                        break;
                }

                operation = node;
                break;
        }

        Intent intent = new Intent(activityContext, activityClass);
        intent.putExtra(AppContext.NODE_OBJECT, operation);
        intent.putExtra(AppContext.OPERATION_ACTION, AppContext.OPERATION_EDIT);
        (activityContext).startActivityForResult(intent, requestCode, ActivityOptionsCompat.makeSceneTransitionAnimation(activityContext).toBundle());
    }

    @Override
    public OperationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        super.onCreateViewHolder(parent, viewType);
        return new OperationViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(OperationViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);

        final Operation operation = adapterList.get(position);

        String subTitle;

        if (operation.getDateTime().get(Calendar.YEAR) == Calendar.getInstance().get(Calendar.YEAR)) {
            subTitle = DateUtils.formatDateTime(activityContext, operation.getDateTime().getTimeInMillis(), DateUtils.FORMAT_SHOW_DATE);
        } else {
            subTitle = DateUtils.formatDateTime(activityContext, operation.getDateTime().getTimeInMillis(), DateUtils.FORMAT_ABBREV_ALL);
        }

        subTitle += ", " + DateUtils.formatDateTime(activityContext, operation.getDateTime().getTimeInMillis(), DateUtils.FORMAT_SHOW_TIME);

        holder.tvOperationSubtitle.setText(subTitle);


        holder.tvOperationConvertCurrency.setVisibility(View.GONE);

        String amountTitle = null;

        switch (operation.getOperationType()) {
            case INCOME:
                IncomeOperation incomeOperation = (IncomeOperation) operation;

                amountTitle = incomeOperation.getFromAmount().toString();
                holder.tvOperationCurrency.setText(incomeOperation.getFromCurrency().getSymbol(LocaleUtils.defaultLocale));
                holder.tvOperationTypeTag.setText(OperationTypeUtils.incomeType.toString());
                holder.tvOperationTypeTag.setBackgroundColor(activityContext.getColor(ColorUtils.incomeColor));
                holder.tvNodeName.setText(incomeOperation.getFromSource().getName() + " -> " + incomeOperation.getToStorage().getName());


                if (incomeOperation.getFromSource().getIconName() == null || IconUtils.getIcon(incomeOperation.getFromSource().getIconName()) == null) {
                    holder.imgNodeIcon.setImageDrawable(ResourcesCompat.getDrawable(activityContext.getResources(), R.drawable.ic_empty, null));
                } else {
                    holder.imgNodeIcon.setImageDrawable(IconUtils.getIcon(incomeOperation.getFromSource().getIconName()));

                }

                break;

            case OUTCOME:
                OutcomeOperation outcomeOperation = (OutcomeOperation) operation;

                amountTitle = outcomeOperation.getFromAmount().toString();
                holder.tvOperationCurrency.setText(outcomeOperation.getFromCurrency().getSymbol(LocaleUtils.defaultLocale));
                holder.tvOperationTypeTag.setText(OperationTypeUtils.outcomeType.toString());
                holder.tvOperationTypeTag.setBackgroundColor(activityContext.getColor(ColorUtils.outcomeColor));
                holder.tvNodeName.setText(outcomeOperation.getFromStorage().getName() + " -> " + outcomeOperation.getToSource().getName());

                if (outcomeOperation.getFromStorage().getIconName() == null || IconUtils.getIcon(outcomeOperation.getFromStorage().getIconName()) == null) {
                    holder.imgNodeIcon.setImageDrawable(ResourcesCompat.getDrawable(activityContext.getResources(), R.drawable.ic_empty, null));
                } else {
                    holder.imgNodeIcon.setImageDrawable(IconUtils.getIcon(outcomeOperation.getFromStorage().getIconName()));
                }

                break;

            case TRANSFER:
                TransferOperation transferOperation = (TransferOperation) operation;

                holder.tvOperationCurrency.setText(transferOperation.getFromCurrency().getSymbol(LocaleUtils.defaultLocale));
                amountTitle = transferOperation.getFromAmount().toString();
                holder.tvOperationTypeTag.setText(OperationTypeUtils.transferType.toString());
                holder.tvOperationTypeTag.setBackgroundColor(activityContext.getColor(ColorUtils.transferColor));
                holder.tvNodeName.setText(transferOperation.getFromStorage().getName() + " -> " + transferOperation.getToStorage().getName());

                if (transferOperation.getToStorage().getIconName() == null || IconUtils.getIcon(transferOperation.getToStorage().getIconName()) == null) {
                    holder.imgNodeIcon.setImageDrawable(ResourcesCompat.getDrawable(activityContext.getResources(), R.drawable.ic_empty, null));
                } else {
                    holder.imgNodeIcon.setImageDrawable(IconUtils.getIcon(transferOperation.getToStorage().getIconName()));
                }

                break;

            case CONVERT:
                ConvertOperation convertOperation = (ConvertOperation) operation;

                holder.tvOperationCurrency.setText(convertOperation.getToCurrency().getSymbol(LocaleUtils.defaultLocale));
                holder.tvOperationTypeTag.setText(OperationTypeUtils.convertType.toString());
                holder.tvOperationTypeTag.setBackgroundColor(activityContext.getColor(ColorUtils.convertColor));
                holder.tvNodeName.setText(convertOperation.getFromStorage().getName() + " -> " + convertOperation.getToStorage().getName());
                holder.tvOperationConvertCurrency.setVisibility(View.VISIBLE);
                holder.tvOperationConvertCurrency.setText(convertOperation.getFromAmount().toString() + " " + convertOperation.getFromCurrency().getSymbol(LocaleUtils.defaultLocale));

                amountTitle = convertOperation.getToAmount().toString();

                holder.tvOperationCurrency.setText(convertOperation.getToCurrency().getSymbol(LocaleUtils.defaultLocale));

                if (convertOperation.getToStorage().getIconName() == null || IconUtils.getIcon(convertOperation.getToStorage().getIconName()) == null) {
                    holder.imgNodeIcon.setImageDrawable(ResourcesCompat.getDrawable(activityContext.getResources(), R.drawable.ic_empty, null));
                } else {
                    holder.imgNodeIcon.setImageDrawable(IconUtils.getIcon(convertOperation.getToStorage().getIconName()));

                }

                break;
        }

        holder.tvOperationAmount.setText(amountTitle);
    }
}
