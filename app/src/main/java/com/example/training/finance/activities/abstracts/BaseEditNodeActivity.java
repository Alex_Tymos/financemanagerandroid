package com.example.training.finance.activities.abstracts;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.training.finance.R;
import com.example.training.finance.activities.SelectIconActivityBase;
import com.example.training.finance.core.storage.interfaces.IconNode;
import com.example.training.finance.transitions.TransitionSlide;
import com.example.training.finance.utils.AppContext;
import com.example.training.finance.utils.IconUtils;

import java.math.BigDecimal;

public abstract class BaseEditNodeActivity<T extends IconNode> extends AppCompatActivity {

    protected Toolbar toolbar;
    protected EditText etName;
    protected TextView tvNodeName;
    protected ImageView imgSave;
    protected ImageView imgClose;
    protected ImageView imgNodeIcon;
    protected T node;
    protected TransitionSlide transition;
    protected String newIconName;
    private int layoutId;

    public BaseEditNodeActivity(int layoutId) {
        this.layoutId = layoutId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layoutId);
        findComponents();
        initListeners();

        node = (T) getIntent().getSerializableExtra(AppContext.NODE_OBJECT);

        changeTitle();
        changeIcon();
        transition = new TransitionSlide(this, TransitionSlide.Direction.BOTTOM_TOP);
    }

    private void findComponents() {
        toolbar = findViewById(R.id.tlb_edit_actions);
        setSupportActionBar(toolbar);
        etName = findViewById(R.id.et_node_name);
        tvNodeName = findViewById(R.id.tv_node_name);
        imgSave = findViewById(R.id.img_node_save);
        imgClose = findViewById(R.id.img_node_close);
        imgNodeIcon = findViewById(R.id.img_node_icon);
    }

    private void initListeners() {

        imgNodeIcon.setOnClickListener(v -> {
            Intent intent = new Intent(BaseEditNodeActivity.this, SelectIconActivityBase.class);
            startActivityForResult(intent, AppContext.REQUEST_SELECT_ICON,
                    ActivityOptionsCompat.makeSceneTransitionAnimation(BaseEditNodeActivity.this).toBundle());
        });

        imgClose.setOnClickListener(v -> transition.finishWithTransition());
    }

    private void changeIcon() {
        if (node.getIconName() == null || IconUtils.getIcon(node.getIconName()) == null) {
            imgNodeIcon.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_empty, null));
        } else {
            imgNodeIcon.setImageDrawable(IconUtils.getIcon(node.getIconName()));
        }
    }

    private void changeTitle() {
        if (node.getName() != null) {
            tvNodeName.setText(R.string.editing);
            etName.setText(node.getName());
        } else {
            tvNodeName.setText(R.string.adding);
            etName.setText("");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == AppContext.REQUEST_SELECT_ICON) {
                newIconName = data.getStringExtra(AppContext.ICON_NAME);

                if (!newIconName.equals(node.getIconName())) {
                    imgNodeIcon.setImageDrawable(IconUtils.getIcon(newIconName));
                }
            }
        }
    }

    protected BigDecimal convertString(String value) {

        if (value.trim().length() != 0) {
            return new BigDecimal(value);
        }

        return BigDecimal.ZERO;
    }
}

