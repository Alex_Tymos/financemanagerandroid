package com.example.training.finance.activities.abstracts;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.training.finance.R;
import com.example.training.finance.activities.list.OperationListActivity;
import com.example.training.finance.activities.list.SourceListActivity;
import com.example.training.finance.activities.list.StorageListActivity;
import com.example.training.finance.transitions.TransitionSlide;

abstract public class BaseDrawerActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    protected TransitionSlide transition;
    // хранит значение, как отработал метод onBackPressed, чтобы понимать что должна делать кнопка back - либо закрывать drawer, либо другое действие, указанное в переопределенном дочернем методе
    protected boolean drawerClosed;
    private Intent sourceListIntent;
    private Intent storageListIntent;
    private Intent operationListIntent;
    private Bundle bundle;
    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sourceListIntent = new Intent(this, SourceListActivity.class);
        storageListIntent = new Intent(this, StorageListActivity.class);
        operationListIntent = new Intent(this, OperationListActivity.class);

        transition = new TransitionSlide(this, TransitionSlide.Direction.BOTTOM_TOP);
        bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(BaseDrawerActivity.this).toBundle();
    }

    public void setTransition(TransitionSlide transition) {
        this.transition = transition;
    }


//    protected void onDrawerShow(){
//
//    }

    protected void createDrawer(Toolbar toolbar) {
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
//            @Override
//            public void onDrawerOpened(View drawerView) {
//                super.onDrawerOpened(drawerView);
//                onDrawerShow(); // если необходимо выполнить какое-то действие при открытии бокового меню
//            }
        };
//        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    // обработка нажатия пунктов drawer
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_operations) {
            startActivity(operationListIntent, bundle);
        } else if (id == R.id.nav_sources) {
            startActivity(sourceListIntent, bundle);
        } else if (id == R.id.nav_storages) {
            startActivity(storageListIntent, bundle);
        } else if (id == R.id.nav_reports) {

        } else if (id == R.id.nav_settings) {

        } else if (id == R.id.nav_help) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            drawerClosed = true;

        } else {
            drawerClosed = false;
        }
    }
}
