package com.example.training.finance.activities.abstracts;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.training.finance.R;
import com.example.training.finance.core.storage.decorator.AbstractSynchronizer;
import com.example.training.finance.core.storage.interfaces.TreeNode;
import com.example.training.finance.fragments.TreeNodeListFragment;
import com.example.training.finance.listeners.TreeNodeActionListener;
import com.example.training.finance.utils.AppContext;
import com.github.clans.fab.FloatingActionButton;

import static com.example.training.finance.utils.AppContext.NODE_OBJECT;
import static com.example.training.finance.utils.AppContext.REQUEST_NODE_ADD_CHILD;

public abstract class TreeListActivity<T extends TreeNode, S extends AbstractSynchronizer<T>> extends BaseListActivity<T, TreeNodeListFragment, S> implements TreeNodeActionListener<T> {

    protected FloatingActionButton fabCreateNode;

    protected int mode;
    protected ImageView icBack;

    protected T swipedNode;// запоминает элемент, над которым показали свайп меню. Нужен будет, если пользователь будет вызывать действия на этом меню, чтобы понимать, над каким компонентом работаем
    protected T parentNode;// запоминает родительский элемент, если он существует (нужно при создании дочернего элемента, чтобы установить parent)


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initToolbar() {
        super.initToolbar();

        mode = getIntent().getIntExtra(AppContext.LIST_VIEW_MODE, 0);

        if (mode == 0) {
            mode = AppContext.EDIT_MODE;
        }

        if (mode == AppContext.EDIT_MODE) {
            super.createDrawer(toolbar); // нужно вызывать, чтобы создать drawer (обязательно нужно передавать не пустой toolbar)
            icBack.setVisibility(View.INVISIBLE);
        } else {
            icBack.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void findComponents() {
        super.findComponents();
        fabCreateNode = findViewById(R.id.fab_create_node);
        fabCreateNode.setShowAnimation(AnimationUtils.loadAnimation(this, R.anim.show_from_bottom));
        fabCreateNode.setHideAnimation(AnimationUtils.loadAnimation(this, R.anim.hide_to_bottom));
        icBack = findViewById(R.id.ic_back_node);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            switch (requestCode) {

                case REQUEST_NODE_ADD_CHILD:
                    addChild((T) data.getSerializableExtra(NODE_OBJECT));
                    break;
            }
        }
    }

    private void addChild(T node) {
        node.setParent(swipedNode);
        fragment.insertChildNode(node);
    }

    @Override
    protected void initListeners() {
        super.initListeners();

        icBack.setOnClickListener(v -> transition.finishWithTransition());
    }

    protected void showParentNodes() {
        if (parentNode.getParent() == null) {
            showRootNodes();
            toolbarTitle.setText(defaultToolbarTitle);

            switch (mode) {
                case AppContext.EDIT_MODE:
                    icBack.setVisibility(View.INVISIBLE);
                    break;
                case AppContext.SELECT_MODE:

                    icBack.setVisibility(View.VISIBLE);
                    icBack.setOnClickListener(v -> transition.finishWithTransition());

                    break;
            }

        } else {
            parentNode = (T) parentNode.getParent();
            fragment.refreshList(parentNode.getChildren(), true);
            toolbarTitle.setText(parentNode.getName());
        }
    }

    protected void showRootNodes() {
        parentNode = null;
    }

    // физическая кнопка
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (!drawerClosed) {
            if (icBack.isShown()) {
                icBack.callOnClick();
            }
        }
    }

    @Override
    protected void addAction(T node) {
        if (parentNode != null) {
            node.setParent(parentNode);
        }
        super.addAction(node);
    }

    // т.к. меню пока не используем - комментируем
//    @Override
//    public void onPopup(T node) {
//        swipedNode = node;// запоминаем, над каким элементом вызвали popup
//    }

    @Override
    public void onShowChilds(T node) {

        parentNode = node;
        toolbarTitle.setText(node.getName());
        icBack.setVisibility(View.VISIBLE);
        icBack.setOnClickListener(v -> showParentNodes()
        );
    }

    @Override
    public void onSwipeOpen(T node) {
        super.onSwipeOpen(node);
        swipedNode = node;
    }

    @Override
    public void returnNodeToOperationActivity(T selectedNode) {
        Intent intent = new Intent();
        intent.putExtra(NODE_OBJECT, selectedNode);
        setResult(RESULT_OK, intent);
        transition.finishWithTransition();
    }

    @Override
    public void onShowSnackBar() {
        fabCreateNode.hide(true);
    }

    @Override
    public void onHideSnackBar() {
        fabCreateNode.show(true);
    }

    @Override
    public void onScroll(RecyclerView recyclerView, int dx, int dy) {
        if (dy > 0) {
            fabCreateNode.hide(true);
        } else if (dy <= 0) {
            fabCreateNode.show(true);
        }
    }
}
