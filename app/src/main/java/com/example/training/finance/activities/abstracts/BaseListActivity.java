package com.example.training.finance.activities.abstracts;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.training.finance.R;
import com.example.training.finance.async.AsyncSearch;
import com.example.training.finance.core.storage.decorator.AbstractSynchronizer;
import com.example.training.finance.core.storage.decorator.SourceSynchronizer;
import com.example.training.finance.core.storage.interfaces.IconNode;
import com.example.training.finance.core.storage.interfaces.Source;
import com.example.training.finance.fragments.BaseNodeListFragment;
import com.example.training.finance.listeners.BaseNodeActionListener;
import com.example.training.finance.transitions.TransitionSlide;
import com.example.training.finance.utils.AppContext;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.List;

import static com.example.training.finance.utils.AppContext.NODE_OBJECT;
import static com.example.training.finance.utils.AppContext.REQUEST_NODE_ADD;
import static com.example.training.finance.utils.AppContext.REQUEST_NODE_EDIT;

public abstract class BaseListActivity<T extends IconNode, F extends BaseNodeListFragment, S extends AbstractSynchronizer<T>> extends BaseDrawerActivity implements BaseNodeActionListener<T> {

    protected Toolbar toolbar;
    protected TextView toolbarTitle;
    protected String searchTitle;
    protected String defaultToolbarTitle;
    protected F fragment;
    protected AsyncSearch<Source, SourceSynchronizer> asyncSearch;
    protected String[] lastQueryParams;
    protected MaterialSearchView searchView;
    protected S sync;
    protected List<T> listBeforeSearch = new ArrayList<>();
    private int listLayoutId;
    private int toolbarLayoutId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(listLayoutId);

        findComponents();

        initToolbar();

        initFragment();

        initListeners();

        TransitionSlide.Direction direction = (TransitionSlide.Direction) getIntent().getSerializableExtra(AppContext.TRANSITION_DIRECTION);

        if (direction != null) {
            setTransition(new TransitionSlide(this, direction));
        }
    }

    protected void findComponents() {

        toolbar = findViewById(toolbarLayoutId);
        toolbarTitle = findViewById(R.id.toolbar_title);
    }

    private void initFragment() {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.node_list_fragment, fragment);
        fragmentTransaction.commit();
    }

    protected void initListeners() {

    }

    public void setToolbarTitle(String title) {
        toolbarTitle.setText(title);
        defaultToolbarTitle = title;
    }

    protected void init(F fragment, int listLayoutId, int toolbarLayoutId, S sync) {
        this.fragment = fragment;
        this.listLayoutId = listLayoutId;
        this.toolbarLayoutId = toolbarLayoutId;
        this.sync = sync;
    }

    protected void initToolbar() {
        setSupportActionBar(toolbar);
        toolbarTitle.setText(defaultToolbarTitle);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        T node;

        if (resultCode == RESULT_OK) {

            switch (requestCode) {
                case REQUEST_NODE_EDIT:
                    fragment.updateNode((T) data.getSerializableExtra(NODE_OBJECT));
                    break;

                case REQUEST_NODE_ADD:
                    node = (T) data.getSerializableExtra(NODE_OBJECT);
                    addAction(node);
                    break;
            }

            if (searchView != null && searchView.isSearchOpen()) {
                search(lastQueryParams);
            }
        }
    }

    protected void addAction(T node) {
        fragment.addNode(node);
    }

    @Override
    public void onAdd(T node) {
    }

    @Override
    public void onSwipeOpen(T node) {
    }

    @Override
    public void onDelete(T node) {
    }

    @Override
    public void onUpdate(T node) {
    }

    @Override
    public void onHideSnackBar() {
    }

    @Override
    public void onShowSnackBar() {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        initSearchView(menu);

        return true;
    }

    protected void initSearchView(Menu menu) {

        searchView = findViewById(R.id.search_view);

        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);

        item.setTitle(searchTitle);
    }

    public void setSearchTitle(String searchTitle) {
        this.searchTitle = searchTitle;
    }

    protected void search(String... queryParams) {
        asyncSearch = new AsyncSearch(sync, this, fragment);
        this.lastQueryParams = queryParams;
        asyncSearch.execute(queryParams);
    }
}

