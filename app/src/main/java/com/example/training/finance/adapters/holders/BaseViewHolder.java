package com.example.training.finance.adapters.holders;


import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.training.finance.R;
import com.malinskiy.superrecyclerview.swipe.BaseSwipeAdapter;
import com.malinskiy.superrecyclerview.swipe.SwipeLayout;

public class BaseViewHolder extends BaseSwipeAdapter.BaseSwipeableViewHolder {

    public final TextView tvNodeName;
    public final ViewGroup layoutMain;
    public final ImageView imgNodeIcon;
    public final View lineSeparator;
    public final SwipeLayout swipeLayout;
    public final ImageView imgSwipeDeleteNode;
    public final ImageView imgSwipeAddChildNode;
    public final ImageView imgSwipeEditNode;

    public BaseViewHolder(View view) {
        super(view);

        tvNodeName = view.findViewById(R.id.tv_node_name);
        layoutMain = (RelativeLayout) view.findViewById(R.id.node_main_layout);

        imgSwipeDeleteNode = view.findViewById(R.id.img_swipe_delete_node);
        imgSwipeAddChildNode = view.findViewById(R.id.img_swipe_add_child_node);
        imgSwipeEditNode = view.findViewById(R.id.img_swipe_edit_node);
        imgNodeIcon = view.findViewById(R.id.img_node_icon);

        lineSeparator = view.findViewById(R.id.line_separator);
        swipeLayout = view.findViewById(R.id.recyclerview_swipe);
    }
}
