package com.example.training.finance.activities.edit_activities.edit_operation;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.training.finance.R;
import com.example.training.finance.activities.abstracts.BaseEditOperationActivity;
import com.example.training.finance.activities.list.SourceListActivity;
import com.example.training.finance.activities.list.StorageListActivity;
import com.example.training.finance.core.storage.enums.OperationType;
import com.example.training.finance.core.storage.impl.operations.IncomeOperation;
import com.example.training.finance.core.storage.interfaces.Source;
import com.example.training.finance.core.storage.interfaces.Storage;
import com.example.training.finance.objects.LocalizedCurrency;
import com.example.training.finance.transitions.TransitionSlide;
import com.example.training.finance.utils.AppContext;
import com.example.training.finance.utils.ColorUtils;
import com.example.training.finance.utils.IconUtils;
import com.example.training.finance.utils.OperationTypeUtils;

import java.util.ArrayList;
import java.util.List;

public class EditIncomeOperationActivity extends BaseEditOperationActivity<IncomeOperation> {


    protected TextView tvOperationSource;
    protected ViewGroup layoutOperationSource;
    protected ImageView icOperationSource;
    protected TextView tvOperationStorage;
    protected ImageView icOperationStorage;
    protected ViewGroup layoutOperationStorage;
    protected EditText etOperationAmount;
    protected Spinner spnCurrency;
    private List<LocalizedCurrency> currencyList = new ArrayList<>();
    private ArrayAdapter<LocalizedCurrency> currencyAdapter;

    public EditIncomeOperationActivity() {
        super(R.layout.activity_edit_income_operation);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initComponents();

        tvOperationType.setText(OperationTypeUtils.incomeType.toString());
        tvOperationType.setBackgroundColor(getBaseContext().getColor(ColorUtils.incomeColor));

        if (actionType == AppContext.OPERATION_EDIT) {
            setValues();
        }

        layoutOperationStorage.setOnClickListener(v -> {

            currentNodeSelect = tvOperationStorage;

            Intent intent = new Intent(EditIncomeOperationActivity.this, StorageListActivity.class);
            intent.putExtra(AppContext.TRANSITION_DIRECTION, TransitionSlide.Direction.RIGHT_LEFT);

            intent.putExtra(AppContext.LIST_VIEW_MODE, AppContext.SELECT_MODE);
            startActivityForResult(intent, REQUEST_SELECT_STORAGE_TO, ActivityOptionsCompat.makeSceneTransitionAnimation(EditIncomeOperationActivity.this).toBundle());
        });

        layoutOperationSource.setOnClickListener(v -> {

            currentNodeSelect = tvOperationSource;

            Intent intent = new Intent(EditIncomeOperationActivity.this, SourceListActivity.class);
            intent.putExtra(AppContext.TRANSITION_DIRECTION, TransitionSlide.Direction.RIGHT_LEFT);

            intent.putExtra(AppContext.LIST_VIEW_MODE, AppContext.SELECT_MODE);

            intent.putExtra(AppContext.LIST_TYPE, OperationType.INCOME.getId());
            startActivityForResult(intent, REQUEST_SELECT_SOURCE_FROM, ActivityOptionsCompat.makeSceneTransitionAnimation(EditIncomeOperationActivity.this).toBundle());
        });

        imgSave.setOnClickListener(v -> {

            if (!checkValues()) {
                return;
            }

            operation.setFromAmount(convertString(etOperationAmount.getText().toString()));
            operation.setDescription(etOperationDesc.getText().toString());
            operation.setDateTime(calendar);
            operation.setFromCurrency(((LocalizedCurrency) spnCurrency.getSelectedItem()).getCurrency());

            Intent intent = new Intent();
            intent.putExtra(AppContext.TRANSITION_DIRECTION, TransitionSlide.Direction.RIGHT_LEFT);
            intent.putExtra(AppContext.NODE_OBJECT, operation);
            setResult(RESULT_OK, intent);

            transition.finishWithTransition();
        });
    }

    private void setValues() {

        initCurrencySpinner();

        tvOperationSource.setText(operation.getFromSource().getName().toUpperCase());
        tvOperationStorage.setText(operation.getToStorage().getName().toUpperCase());
        etOperationAmount.setText(operation.getFromAmount().toString());

        icOperationSource.setImageDrawable(IconUtils.getIcon(operation.getFromSource().getIconName()));
        icOperationStorage.setImageDrawable(IconUtils.getIcon(operation.getToStorage().getIconName()));

        tvOperationStorage.setTextColor(getResources().getColor(R.color.colorText, null));
        tvOperationSource.setTextColor(getResources().getColor(R.color.colorText, null));
    }

    private void initComponents() {
        icOperationSource = findViewById(R.id.ic_operation_source_selected);
        icOperationStorage = findViewById(R.id.ic_operation_storage_selected);

        tvOperationSource = findViewById(R.id.tv_operation_source_selected);
        tvOperationStorage = findViewById(R.id.tv_operation_storage_selected);
        etOperationAmount = findViewById(R.id.et_operation_amount_selected);

        layoutOperationSource = findViewById(R.id.layout_operation_source);
        layoutOperationStorage = findViewById(R.id.layout_operation_storage);

        spnCurrency = findViewById(R.id.spn_currency);
        currencyAdapter = new ArrayAdapter<>(this, R.layout.spinner_currency_item, currencyList);
        spnCurrency.setAdapter(currencyAdapter);
    }

    private boolean checkValues() {

        if (etOperationAmount.getText().length() == 0) {
            Toast.makeText(EditIncomeOperationActivity.this, R.string.enter_amount, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (operation.getFromSource() == null) {
            Toast.makeText(EditIncomeOperationActivity.this, R.string.select_source_from, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (operation.getToStorage() == null) {
            Toast.makeText(EditIncomeOperationActivity.this, R.string.select_storage_to, Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private void initCurrencySpinner() {

        currencyList.clear();
        currencyList.addAll(getLocalizedCurrencyList(operation.getToStorage().getAvailableCurrencies()));

        currencyAdapter.clear();
        currencyAdapter.addAll(getLocalizedCurrencyList(operation.getToStorage().getAvailableCurrencies()));

        currencyAdapter.notifyDataSetChanged();
        spnCurrency.setSelection(currencyList.indexOf(operation.getFromCurrency()));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            switch (requestCode) {
                case REQUEST_SELECT_STORAGE_TO:
                    Storage storage = (Storage) data.getSerializableExtra(AppContext.NODE_OBJECT);
                    operation.setToStorage(storage);
                    icOperationStorage.setImageDrawable(IconUtils.getIcon(storage.getIconName()));
                    currentNodeSelect.setText(storage.getName().toUpperCase());
                    tvOperationStorage.setTextColor(getResources().getColor(R.color.colorText, null));

                    updateCurrencyList(storage, currencyList, currencyAdapter, spnCurrency);

                    break;

                case REQUEST_SELECT_SOURCE_FROM: // если выбирали source
                    Source source = (Source) data.getSerializableExtra(AppContext.NODE_OBJECT);
                    operation.setFromSource(source);
                    icOperationSource.setImageDrawable(IconUtils.getIcon(source.getIconName()));
                    currentNodeSelect.setText(source.getName().toUpperCase());
                    tvOperationSource.setTextColor(getResources().getColor(R.color.colorText, null));

                    break;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (operation.getToStorage() != null) {
            updateCurrencyList(operation.getToStorage(), currencyList, currencyAdapter, spnCurrency);
        }
    }
}
