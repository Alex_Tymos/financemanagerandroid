package com.example.training.finance.adapters.abstracts;


import android.app.Activity;
import android.support.design.widget.Snackbar;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.training.finance.R;
import com.example.training.finance.adapters.holders.BaseViewHolder;
import com.example.training.finance.core.storage.decorator.AbstractSynchronizer;
import com.example.training.finance.core.storage.interfaces.IconNode;
import com.example.training.finance.listeners.BaseNodeActionListener;
import com.example.training.finance.utils.AppContext;
import com.example.training.finance.utils.IconUtils;
import com.malinskiy.superrecyclerview.swipe.BaseSwipeAdapter;
import com.malinskiy.superrecyclerview.swipe.SimpleSwipeListener;
import com.malinskiy.superrecyclerview.swipe.SwipeLayout;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import jp.wasabeef.recyclerview.animators.BaseItemAnimator;
import jp.wasabeef.recyclerview.animators.FadeInAnimator;

public abstract class BaseNodeListAdapter<T extends IconNode, VH extends BaseViewHolder, L extends BaseNodeActionListener<T>> extends BaseSwipeAdapter<VH> {

    protected static final String TAG = BaseNodeListAdapter.class.getName();
    public static BaseItemAnimator animatorChilds;
    public static BaseItemAnimator animatorParents;
    protected Comparator comparator;
    protected List<T> adapterList = new ArrayList<>();
    protected RecyclerView recyclerView;
    protected AbstractSynchronizer sync;
    protected Snackbar snackbar;
    protected Activity activityContext;
    protected int currentEditPosition;
    protected View itemView;
    protected L listener;
    protected int itemLayoutId;

    public BaseNodeListAdapter(AbstractSynchronizer sync, int itemLayoutId) {
        this.sync = sync;
        this.itemLayoutId = itemLayoutId;
        adapterList = sync.getAll();

    }

    public BaseNodeListAdapter(AbstractSynchronizer sync, int itemLayoutId, List<T> initialList) {
        this.sync = sync;
        this.itemLayoutId = itemLayoutId;
        adapterList = initialList;
    }

    protected abstract void openActivityOnClick(T node, int requestCode);

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext()).inflate(itemLayoutId, parent, false);

        activityContext = (Activity) parent.getContext();

        recyclerView = (RecyclerView) parent;

        createAnimations();

        return null;
    }

    @Override
    public void onBindViewHolder(VH holder, final int position) {
        super.onBindViewHolder(holder, position);

        final T node = adapterList.get(position);

        final SwipeLayout swipeLayout = holder.swipeLayout;

        swipeLayout.setDragEdge(SwipeLayout.DragEdge.Right);
        swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);

        closeItem(position);

        swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {
                listener.onSwipeOpen(node);
            }
        });

        initListeners(holder, position, node);

        holder.tvNodeName.setText(node.getName());

        if (node.getIconName() == null || IconUtils.getIcon(node.getIconName()) == null) {
            holder.imgNodeIcon.setImageDrawable(ResourcesCompat.getDrawable(activityContext.getResources(), R.drawable.ic_empty, null));
        } else {
            holder.imgNodeIcon.setImageDrawable(IconUtils.getIcon(node.getIconName()));

        }

        if (position == adapterList.size() - 1) {
            holder.lineSeparator.setVisibility(View.GONE);
        } else {
            holder.lineSeparator.setVisibility(View.VISIBLE);
        }
    }

    protected void initListeners(VH holder, final int position, final T node) {

        holder.layoutMain.setOnClickListener(v -> {

            closeItem(position);

            nodeClickAction(position, node);
        });

        holder.imgSwipeDeleteNode.setOnClickListener(v -> {
            closeItem(position);
            closeSnackBar();
            deleteWithSnackbar(node, position);
        });

        holder.imgSwipeEditNode.setOnClickListener(v -> runEditActivity(position, node));
    }

    public void closeSwipeLayouts() {
        for (SwipeLayout s : getOpenLayouts()) {
            s.close();
        }
    }

    private void createAnimations() {

        animatorParents = new BaseItemAnimator() {

            @Override
            protected void animateRemoveImpl(final RecyclerView.ViewHolder holder) {
                ViewCompat.animate(holder.itemView)
                        .translationX(holder.itemView.getRootView().getWidth())
                        .setDuration(200)
                        .setInterpolator(mInterpolator)
                        .setListener(new DefaultRemoveVpaListener(holder))
                        .setStartDelay(getRemoveDelay(holder))
                        .start();
            }

            @Override
            protected void preAnimateAddImpl(RecyclerView.ViewHolder holder) {
                ViewCompat.setTranslationX(holder.itemView, -holder.itemView.getRootView().getWidth());
            }

            @Override
            protected void animateAddImpl(final RecyclerView.ViewHolder holder) {
                ViewCompat.animate(holder.itemView)
                        .translationX(0)
                        .setDuration(200)
                        .setInterpolator(mInterpolator)
                        .setListener(new DefaultAddVpaListener(holder))
                        .setStartDelay(getAddDelay(holder))
                        .start();
            }
        };


        animatorChilds = new BaseItemAnimator() {

            @Override
            protected void animateRemoveImpl(final RecyclerView.ViewHolder holder) {

                ViewCompat.animate(holder.itemView)
                        .translationX(-holder.itemView.getRootView().getWidth())
                        .setDuration(200)
                        .setInterpolator(mInterpolator)
                        .setListener(new DefaultRemoveVpaListener(holder))
                        .setStartDelay(getRemoveDelay(holder))
                        .start();
            }

            @Override
            protected void preAnimateAddImpl(RecyclerView.ViewHolder holder) {
                ViewCompat.setTranslationX(holder.itemView, holder.itemView.getRootView().getWidth());
            }

            @Override
            protected void animateAddImpl(final RecyclerView.ViewHolder holder) {
                ViewCompat.animate(holder.itemView)
                        .translationX(0)
                        .setDuration(200)
                        .setInterpolator(mInterpolator)
                        .setListener(new DefaultAddVpaListener(holder))
                        .setStartDelay(getAddDelay(holder))
                        .start();
            }
        };
    }


    @Override
    public int getItemCount() {
        return adapterList.size();
    }

    protected void closeSnackBar() {
        if (snackbar != null && snackbar.isShown()) {
            snackbar.dismiss();
        }
    }

    protected void nodeClickAction(int position, T node) {
        runEditActivity(position, node);
    }

    protected void runEditActivity(int position, T node) {

        closeSwipeLayouts();

        closeSnackBar();
        currentEditPosition = position;

        openActivityOnClick(node, AppContext.REQUEST_NODE_EDIT);
    }

    protected void deleteWithSnackbar(final T node, final int position) {

        closeSnackBar();

        adapterList.remove(node);
//                        notifyItemRemoved(position);
        // TODO реализовать обновление position для всех элементов - т.к. есть некоторые глюки при обновлении позиций, если вызывать notifyItemRemoved, тогда как notifyDataSetChanged правильно обновляет индексы для всех записей списка

        notifyDataSetChanged();

        snackbar = Snackbar.make(recyclerView, R.string.deleted, Snackbar.LENGTH_LONG);

        listener.onShowSnackBar();

        snackbar.setAction(R.string.undo, v -> {
            closeSwipeLayouts();
            adapterList.add(position, node);
//                                notifyItemInserted(position);
            listener.onAdd(node);
            notifyDataSetChanged();

        }).setCallback(new Snackbar.Callback() {

            @Override
            public void onDismissed(Snackbar snackbar, int event) {

                listener.onHideSnackBar();

                if (event != DISMISS_EVENT_ACTION) {
                    deleteNode(node, position);
                }
            }
        }).show();
    }

    public void refreshList(final List<T> list, RecyclerView.ItemAnimator animator) {

        if (recyclerView == null) {
            return;
        }

        closeSwipeLayouts();

        if (snackbar != null && snackbar.isShown()) {
            closeSnackBar();
        }

        if (animator == null) {
            recyclerView.setItemAnimator(new FadeInAnimator());
            adapterList = list;
            notifyDataSetChanged();
            return;
        }

        recyclerView.setItemAnimator(animator);

        if (list.isEmpty()) {
            int range = adapterList.size();
            adapterList = list;
            notifyItemRangeRemoved(0, range);
            return;
        }

        int range = adapterList.size();
        notifyItemRangeRemoved(0, range);

        adapterList = list;

        if (adapterList.size() == 1) {
            notifyItemInserted(0);
        } else {
            notifyItemRangeInserted(0, adapterList.size());
        }
    }

    public void addNode(T node) {
        try {
            closeSwipeLayouts();

            // TODO переделать, чтобы при вставке объект сразу попадал в нужный индекс (чтобы не сортировать каждый раз)

            sync.add(node);

            if (comparator != null) {
                Collections.sort(adapterList, comparator);
            }

//            notifyItemInserted(adapterList.size() - 1);// вставка в последнюю позицию
//            recyclerView.scrollToPosition(adapterList.size() - 1);

            listener.onAdd(node);

            notifyDataSetChanged();

        } catch (SQLException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    public void updateNode(T node) {
        try {
            closeSwipeLayouts();
            sync.update(node);

            if (comparator != null) {
                Collections.sort(adapterList, comparator);
            }

            listener.onUpdate(node);

//            notifyItemChanged(currentEditPosition);
            notifyDataSetChanged();

        } catch (SQLException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    public boolean deleteNode(T node, int position) {
        try {
            closeItem(position);


            sync.delete(node);
            notifyDataSetChanged();

            listener.onDelete(node);

            return true;

        } catch (SQLException e) {
            Log.e(TAG, e.getMessage());
        }

        return false;
    }

    public void setListener(L listener) {
        this.listener = listener;
    }

    public void setContext(Activity activityContext) {
        this.activityContext = activityContext;
    }

    public List<T> getAdapterList() {
        return adapterList;
    }
}
