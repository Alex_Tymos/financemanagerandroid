package com.example.training.finance.activities.list;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

import com.example.training.finance.R;
import com.example.training.finance.activities.abstracts.TreeListActivity;
import com.example.training.finance.activities.edit_activities.EditSourceActivity;
import com.example.training.finance.adapters.SourceNodeAdapter;
import com.example.training.finance.core.storage.database.Initializer;
import com.example.training.finance.core.storage.decorator.SourceSynchronizer;
import com.example.training.finance.core.storage.enums.OperationType;
import com.example.training.finance.core.storage.impl.DefaultSource;
import com.example.training.finance.core.storage.interfaces.Source;
import com.example.training.finance.fragments.TreeNodeListFragment;
import com.example.training.finance.utils.AppContext;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

public class SourceListActivity extends TreeListActivity<Source, SourceSynchronizer> {

    private TabLayout tabLayout;
    private OperationType listFilterType;
    private Source oldSelectedParentNode;
    private OperationType defaultSourceType;

    public SourceListActivity() {

        TreeNodeListFragment<Source> fragment = new TreeNodeListFragment<>();
        init(fragment, R.layout.activity_source_list, R.id.tlb_tree_list_actions, Initializer.getSourceSynchronizer());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        listFilterType = OperationType.getType(getIntent().getIntExtra(AppContext.LIST_TYPE, -1));

        defaultSourceType = listFilterType;

        if (listFilterType == null) {
            fragment.setAdapter(new SourceNodeAdapter(mode));
        } else {
            fragment.setAdapter(new SourceNodeAdapter(mode, Initializer.getSourceSynchronizer().getList(listFilterType)));
        }

        setToolbarTitle(getResources().getString(R.string.sources));

        setSearchTitle(getResources().getString(R.string.source_search_hint));
    }

    @Override
    protected void initToolbar() {
        super.initToolbar();

        tabLayout = findViewById(R.id.tabs);

        if (mode == AppContext.EDIT_MODE) {
            tabLayout.setVisibility(View.VISIBLE);
            initTabs();
        } else {
            tabLayout.setVisibility(View.GONE);
        }
    }

    @Override
    protected void initListeners() {
        super.initListeners();

        fabCreateNode.setOnClickListener(v -> {

            Source source = new DefaultSource();

            if (defaultSourceType != null) {
                source.setOperationType(defaultSourceType);
            } else {
                if (parentNode != null) {
                    source.setOperationType(parentNode.getOperationType());
                }
            }

            Intent intent = new Intent(SourceListActivity.this, EditSourceActivity.class);
            intent.putExtra(AppContext.NODE_OBJECT, source);
            startActivityForResult(intent, AppContext.REQUEST_NODE_ADD, ActivityOptionsCompat.makeSceneTransitionAnimation(SourceListActivity.this).toBundle());
        });
    }

    private void initTabs() {

        tabLayout.getTabAt(0).setText(R.string.tab_all);
        tabLayout.getTabAt(1).setText(R.string.tab_income);
        tabLayout.getTabAt(2).setText(R.string.tab_outcome);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                icBack.setVisibility(View.INVISIBLE);
                toolbarTitle.setText(R.string.sources);
                parentNode = null;

                if (mode == AppContext.EDIT_MODE) {

                    switch (tabLayout.getSelectedTabPosition()) {
                        case 0:// все
                            fragment.refreshList(Initializer.getSourceSynchronizer().getAll(), false);
                            defaultSourceType = null;
                            break;
                        case 1:// доход
                            fragment.refreshList(Initializer.getSourceSynchronizer().getList(OperationType.INCOME), false);
                            defaultSourceType = OperationType.INCOME;
                            break;
                        case 2: // расход
                            fragment.refreshList(Initializer.getSourceSynchronizer().getList(OperationType.OUTCOME), false);
                            defaultSourceType = OperationType.OUTCOME;
                            break;
                    }
                } else {
                    fragment.refreshList(Initializer.getSourceSynchronizer().getList(listFilterType), false);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    @Override
    protected void showRootNodes() {
        super.showRootNodes();
        parentNode = null;
        if (mode == AppContext.EDIT_MODE) {

            switch (tabLayout.getSelectedTabPosition()) {
                case 0:// все
                    fragment.refreshList(Initializer.getSourceSynchronizer().getAll(), true);
                    defaultSourceType = null;
                    break;
                case 1:// доход
                    fragment.refreshList(Initializer.getSourceSynchronizer().getList(OperationType.INCOME), true);
                    defaultSourceType = OperationType.INCOME;
                    break;
                case 2: // расход
                    fragment.refreshList(Initializer.getSourceSynchronizer().getList(OperationType.OUTCOME), true);
                    defaultSourceType = OperationType.OUTCOME;
                    break;
            }
        } else {
            fragment.refreshList(Initializer.getSourceSynchronizer().getList(listFilterType), true);
        }
    }

    @Override
    protected void showParentNodes() {

        super.showParentNodes();

    }

    @Override
    public void onSwipeOpen(Source node) {
        super.onSwipeOpen(node);
        defaultSourceType = node.getOperationType();
    }

    @Override
    public void onShowChilds(Source node) {
        super.onShowChilds(node);
        defaultSourceType = node.getOperationType();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        initSearchView(menu);

        return true;
    }

    @Override
    protected void initSearchView(Menu menu) {
        super.initSearchView(menu);

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                searchView.clearFocus();

                listBeforeSearch.clear();
                listBeforeSearch.addAll(fragment.getList());
                oldSelectedParentNode = parentNode;

                if (query.trim().length() == 0) {
                    Toast.makeText(SourceListActivity.this, R.string.enter_search_string, Toast.LENGTH_SHORT).show();
                    return false;
                }

                if (listFilterType == null) {
                    search(query);
                } else {
                    search(new String[]{query, String.valueOf(listFilterType.getId())});
                }

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                if (mode == AppContext.EDIT_MODE) {
                    fabCreateNode.hide(true);
                    tabLayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSearchViewClosed() {
                if (mode == AppContext.EDIT_MODE) {
                    tabLayout.setVisibility(View.VISIBLE);
                }

                if (asyncSearch != null && asyncSearch.isSearched()) {
                    parentNode = oldSelectedParentNode;
                    fabCreateNode.show(true);
                    asyncSearch = null;
                    fragment.refreshList(listBeforeSearch, false);
                }
            }
        });
    }
}
