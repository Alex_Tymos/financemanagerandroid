package com.example.training.finance.activities.edit_activities.edit_operation;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.training.finance.R;
import com.example.training.finance.activities.abstracts.BaseEditOperationActivity;
import com.example.training.finance.activities.list.StorageListActivity;
import com.example.training.finance.core.storage.impl.operations.ConvertOperation;
import com.example.training.finance.core.storage.interfaces.Storage;
import com.example.training.finance.objects.LocalizedCurrency;
import com.example.training.finance.transitions.TransitionSlide;
import com.example.training.finance.utils.AppContext;
import com.example.training.finance.utils.ColorUtils;
import com.example.training.finance.utils.IconUtils;
import com.example.training.finance.utils.OperationTypeUtils;

import java.util.ArrayList;
import java.util.List;

public class EditConvertOperationActivity extends BaseEditOperationActivity<ConvertOperation> {

    protected TextView tvOperationStorageFrom;
    protected ViewGroup layoutOperationStorageFrom;
    protected ImageView icOperationStorageFrom;
    protected TextView tvOperationStorageTo;
    protected ViewGroup layoutOperationStorageTo;
    protected ImageView icOperationStorageTo;
    protected EditText etOperationAmountFrom;
    protected Spinner spnCurrencyFrom;
    protected EditText etOperationAmountTo;
    protected Spinner spnCurrencyTo;
    private List<LocalizedCurrency> currencyListFrom = new ArrayList<>();
    private ArrayAdapter<LocalizedCurrency> currencyAdapterFrom;
    private List<LocalizedCurrency> currencyListTo = new ArrayList<>();
    private ArrayAdapter<LocalizedCurrency> currencyAdapterTo;

    public EditConvertOperationActivity() {
        super(R.layout.activity_edit_convert_operation);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initComponents();

        tvOperationType.setText(OperationTypeUtils.convertType.toString());
        tvOperationType.setBackgroundColor(getBaseContext().getColor(ColorUtils.convertColor));

        if (actionType == AppContext.OPERATION_EDIT) {
            initValues();
        }

        layoutOperationStorageFrom.setOnClickListener(v -> {

            currentNodeSelect = tvOperationStorageFrom;

            Intent intent = new Intent(EditConvertOperationActivity.this, StorageListActivity.class);
            intent.putExtra(AppContext.TRANSITION_DIRECTION, TransitionSlide.Direction.RIGHT_LEFT);
            intent.putExtra(AppContext.LIST_VIEW_MODE, AppContext.SELECT_MODE);
            startActivityForResult(intent, REQUEST_SELECT_STORAGE_FROM, ActivityOptionsCompat.makeSceneTransitionAnimation(EditConvertOperationActivity.this).toBundle());
        });

        layoutOperationStorageTo.setOnClickListener(v -> {

            currentNodeSelect = tvOperationStorageTo;

            Intent intent = new Intent(EditConvertOperationActivity.this, StorageListActivity.class);
            intent.putExtra(AppContext.TRANSITION_DIRECTION, TransitionSlide.Direction.RIGHT_LEFT);
            intent.putExtra(AppContext.LIST_VIEW_MODE, AppContext.SELECT_MODE);
            startActivityForResult(intent, REQUEST_SELECT_STORAGE_TO, ActivityOptionsCompat.makeSceneTransitionAnimation(EditConvertOperationActivity.this).toBundle());


        });

        imgSave.setOnClickListener(v -> {

            if (!checkValues()) {
                return;
            }

            operation.setFromAmount(convertString(etOperationAmountFrom.getText().toString()));
            operation.setToAmount(convertString(etOperationAmountTo.getText().toString()));
            operation.setDescription(etOperationDesc.getText().toString());
            operation.setFromCurrency(((LocalizedCurrency) spnCurrencyFrom.getSelectedItem()).getCurrency());
            operation.setToCurrency(((LocalizedCurrency) spnCurrencyTo.getSelectedItem()).getCurrency());
            operation.setDateTime(calendar);

            Intent intent = new Intent();
            intent.putExtra(AppContext.TRANSITION_DIRECTION, TransitionSlide.Direction.RIGHT_LEFT);
            intent.putExtra(AppContext.NODE_OBJECT, operation);
            setResult(RESULT_OK, intent);

            transition.finishWithTransition();

        });
    }

    private void initValues() {

        initCurrencySpinner();

        tvOperationStorageFrom.setText(operation.getFromStorage().getName().toUpperCase());
        icOperationStorageFrom.setImageDrawable(IconUtils.getIcon(operation.getFromStorage().getIconName()));
        etOperationAmountFrom.setText(operation.getFromAmount().toString());
        etOperationAmountTo.setText(operation.getToAmount().toString());
        icOperationStorageTo.setImageDrawable(IconUtils.getIcon(operation.getToStorage().getIconName()));
        tvOperationStorageTo.setText(operation.getToStorage().getName().toUpperCase());
        tvOperationStorageFrom.setTextColor(getResources().getColor(R.color.colorText, null));
        tvOperationStorageTo.setTextColor(getResources().getColor(R.color.colorText, null));
    }

    private void initComponents() {
        layoutOperationStorageFrom = findViewById(R.id.layout_operation_from_storage);
        icOperationStorageFrom = findViewById(R.id.ic_operation_from_storage_selected);
        tvOperationStorageFrom = findViewById(R.id.tv_operation_from_storage_selected);
        etOperationAmountFrom = findViewById(R.id.et_operation_from_amount_selected);
        layoutOperationStorageTo = findViewById(R.id.layout_operation_to_storage);
        etOperationAmountTo = findViewById(R.id.et_operation_to_amount_selected);
        icOperationStorageTo = findViewById(R.id.ic_operation_to_storage_selected);
        tvOperationStorageTo = findViewById(R.id.tv_operation_to_storage_selected);

        spnCurrencyFrom = findViewById(R.id.spn_from_currency);
        spnCurrencyTo = findViewById(R.id.spn_to_currency);

        currencyAdapterFrom = new ArrayAdapter<>(this, R.layout.spinner_currency_item, currencyListFrom);
        spnCurrencyFrom.setAdapter(currencyAdapterFrom);

        currencyAdapterTo = new ArrayAdapter<>(this, R.layout.spinner_currency_item, currencyListTo);
        spnCurrencyTo.setAdapter(currencyAdapterTo);
    }

    private boolean checkValues() {

        if (etOperationAmountFrom.getText().length() == 0) {
            Toast.makeText(EditConvertOperationActivity.this, R.string.enter_amount_from, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (etOperationAmountTo.getText().length() == 0) {
            Toast.makeText(EditConvertOperationActivity.this, R.string.enter_amount_to, Toast.LENGTH_SHORT).show();
            return false;
        }


        if (operation.getFromStorage() == null) {
            Toast.makeText(EditConvertOperationActivity.this, R.string.select_storage_from, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (operation.getToStorage() == null) {
            Toast.makeText(EditConvertOperationActivity.this, R.string.select_storage_to, Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }


    private void initCurrencySpinner() {

        currencyAdapterFrom.clear();
        currencyAdapterFrom.addAll(getLocalizedCurrencyList(operation.getFromStorage().getAvailableCurrencies()));

        currencyAdapterTo.clear();
        currencyAdapterTo.addAll(getLocalizedCurrencyList(operation.getToStorage().getAvailableCurrencies()));

        currencyAdapterTo.notifyDataSetChanged();
        currencyAdapterFrom.notifyDataSetChanged();

        spnCurrencyFrom.setSelection(currencyListFrom.indexOf(operation.getFromCurrency()));
        spnCurrencyTo.setSelection(currencyListTo.indexOf(operation.getToCurrency()));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            Storage storage = (Storage) data.getSerializableExtra(AppContext.NODE_OBJECT);

            switch (requestCode) {
                case REQUEST_SELECT_STORAGE_FROM:
                    operation.setFromStorage(storage);
                    icOperationStorageFrom.setImageDrawable(IconUtils.getIcon(storage.getIconName()));
                    currentNodeSelect.setText(storage.getName().toUpperCase());

                    updateCurrencyList(storage, currencyListFrom, currencyAdapterFrom, spnCurrencyFrom);
                    tvOperationStorageFrom.setTextColor(getResources().getColor(R.color.colorText, null));

                    break;

                case REQUEST_SELECT_STORAGE_TO:
                    operation.setToStorage(storage);
                    icOperationStorageTo.setImageDrawable(IconUtils.getIcon(storage.getIconName()));
                    currentNodeSelect.setText(storage.getName().toUpperCase());
                    tvOperationStorageTo.setTextColor(getResources().getColor(R.color.colorText, null));

                    updateCurrencyList(storage, currencyListTo, currencyAdapterTo, spnCurrencyTo);

                    break;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (operation.getToStorage() != null) {
            updateCurrencyList(operation.getToStorage(), currencyListTo, currencyAdapterTo, spnCurrencyTo);
        }

        if (operation.getFromStorage() != null) {
            updateCurrencyList(operation.getFromStorage(), currencyListFrom, currencyAdapterFrom, spnCurrencyFrom);
        }
    }
}

