package com.example.training.finance.fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.training.finance.R;
import com.example.training.finance.adapters.abstracts.BaseNodeListAdapter;
import com.example.training.finance.core.storage.interfaces.IconNode;
import com.example.training.finance.listeners.BaseNodeActionListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;

import java.util.List;

public class BaseNodeListFragment<T extends IconNode, A extends BaseNodeListAdapter, L extends BaseNodeActionListener> extends Fragment {

    protected A adapter;
    protected SuperRecyclerView recyclerView;
    protected View view;
    private L clickListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.node_list, container, false);

        if (view instanceof SuperRecyclerView) {
            Context context = view.getContext();
            recyclerView = (SuperRecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));

            recyclerView.setAdapter(adapter);

            recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    if (recyclerView.getScrollState() > 0) {
                        clickListener.onScroll(recyclerView, dx, dy);
                    }
                }

                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

                }
            });

        }
        return view;
    }

    public void refreshList(List<T> list, boolean animation) {
        if (animation) {
            adapter.refreshList(list, BaseNodeListAdapter.animatorParents);
        } else {
            adapter.refreshList(list, null);

        }
    }

    public void updateNode(T node) {
        adapter.updateNode(node);
    }

    public void addNode(T node) {
        adapter.addNode(node);
    }

    public List<T> getList() {
        return adapter.getAdapterList();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof BaseNodeActionListener) {
            clickListener = (L) context;
            adapter.setListener(clickListener);
            adapter.setContext((Activity) getContext());
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement NodeActionListener");
        }
    }

    public SuperRecyclerView getRecyclerView() {
        return recyclerView;
    }

    public void setAdapter(A adapter) {
        this.adapter = adapter;
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.closeSwipeLayouts();
    }
}
