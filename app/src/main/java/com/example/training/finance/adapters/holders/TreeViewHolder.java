package com.example.training.finance.adapters.holders;


import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.training.finance.R;

public class TreeViewHolder extends BaseViewHolder {

    public final ImageView btnPopup;
    public final TextView tvChildCount;
    public final ViewGroup layoutShowChilds;

    public TreeViewHolder(View view) {
        super(view);

        btnPopup = view.findViewById(R.id.img_node_popup);
        tvChildCount = view.findViewById(R.id.tv_node_child_count);
        layoutShowChilds = view.findViewById(R.id.layout_show_childs);
    }
}
