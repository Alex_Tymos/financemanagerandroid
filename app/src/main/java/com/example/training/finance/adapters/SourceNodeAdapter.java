package com.example.training.finance.adapters;


import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.ViewGroup;

import com.example.training.finance.activities.edit_activities.EditSourceActivity;
import com.example.training.finance.adapters.abstracts.TreeNodeListAdapter;
import com.example.training.finance.adapters.holders.SourceViewHolder;
import com.example.training.finance.core.storage.database.Initializer;
import com.example.training.finance.core.storage.impl.DefaultSource;
import com.example.training.finance.core.storage.interfaces.Source;
import com.example.training.finance.utils.AppContext;

import java.util.List;

public class SourceNodeAdapter extends TreeNodeListAdapter<Source, SourceViewHolder> {

    private static final String TAG = SourceNodeAdapter.class.getName();

    public SourceNodeAdapter(int mode) {
        super(mode, Initializer.getSourceSynchronizer(), Initializer.getSourceSynchronizer().getAll());
    }

    public SourceNodeAdapter(int mode, List<Source> initialList) {
        super(mode, Initializer.getSourceSynchronizer(), initialList);

    }

    @Override
    public SourceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        super.onCreateViewHolder(parent, viewType);
        return new SourceViewHolder(itemView);
    }

    @Override
    protected void openActivityOnClick(Source source, int requestCode) {

        Source s;

        switch (requestCode) {
            case AppContext.REQUEST_NODE_ADD_CHILD:
                s = new DefaultSource();
                s.setOperationType(source.getOperationType());
                break;

            default:
                s = source;
        }

        Intent intent = new Intent(activityContext, EditSourceActivity.class);
        intent.putExtra(AppContext.NODE_OBJECT, s);
        activityContext.startActivityForResult(intent, requestCode, ActivityOptionsCompat.makeSceneTransitionAnimation(activityContext).toBundle());
    }
}

