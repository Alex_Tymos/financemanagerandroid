package com.example.training.finance.listeners;


public interface TreeNodeActionListener<T> extends BaseNodeActionListener<T> {

    void returnNodeToOperationActivity(T node);

    void onShowChilds(T node);
}
