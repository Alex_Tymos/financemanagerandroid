package com.example.training.finance.async;


import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.training.finance.R;
import com.example.training.finance.core.storage.decorator.AbstractSynchronizer;
import com.example.training.finance.core.storage.interfaces.IconNode;
import com.example.training.finance.fragments.BaseNodeListFragment;

import java.util.List;

public class AsyncSearch<T extends IconNode, S extends AbstractSynchronizer<T>> extends AsyncTask<String, String, List<T>> {

    private Context context;
    private BaseNodeListFragment fragment;
    private S sync;
    private boolean searched;

    public AsyncSearch(S sync, Context context, BaseNodeListFragment fragment) {
        this.sync = sync;
        this.context = context;
        this.fragment = fragment;
    }

    @Override
    protected void onPreExecute() {

        fragment.getRecyclerView().showProgress();
    }


    @Override
    protected List<T> doInBackground(String... params) {
        List<T> list = sync.search(params);
        return list;
    }

    @Override
    protected void onPostExecute(List<T> list) {
        fragment.getRecyclerView().hideProgress();
        fragment.getRecyclerView().showRecycler();
        fragment.refreshList(list, false);

        searched = true;

        if (list.isEmpty()) {
            Toast.makeText(context, R.string.not_found, Toast.LENGTH_SHORT).show();
            return;
        }
    }

    public boolean isSearched() {
        return searched;
    }
}
