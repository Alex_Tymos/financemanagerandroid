package com.example.training.finance.adapters.holders;


import android.view.View;
import android.widget.TextView;

import com.example.training.finance.R;

public class StorageViewHolder extends TreeViewHolder {


    public final TextView tvAmount;


    public StorageViewHolder(View view) {
        super(view);
        tvAmount = view.findViewById(R.id.tv_node_amount);
        tvAmount.setVisibility(View.VISIBLE);

    }

}
