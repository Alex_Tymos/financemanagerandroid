package com.example.training.finance.activities.list;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.Menu;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.example.training.finance.R;
import com.example.training.finance.activities.abstracts.BaseListActivity;
import com.example.training.finance.activities.edit_activities.edit_operation.EditConvertOperationActivity;
import com.example.training.finance.activities.edit_activities.edit_operation.EditIncomeOperationActivity;
import com.example.training.finance.activities.edit_activities.edit_operation.EditOutcomeOperationActivity;
import com.example.training.finance.activities.edit_activities.edit_operation.EditTransferOperationActivity;
import com.example.training.finance.adapters.OperationListAdapter;
import com.example.training.finance.core.storage.database.Initializer;
import com.example.training.finance.core.storage.decorator.OperationSynchronizer;
import com.example.training.finance.core.storage.impl.operations.ConvertOperation;
import com.example.training.finance.core.storage.impl.operations.IncomeOperation;
import com.example.training.finance.core.storage.impl.operations.OutcomeOperation;
import com.example.training.finance.core.storage.impl.operations.TransferOperation;
import com.example.training.finance.core.storage.interfaces.Operation;
import com.example.training.finance.fragments.BaseNodeListFragment;
import com.example.training.finance.listeners.BaseNodeActionListener;
import com.example.training.finance.transitions.TransitionSlide;
import com.example.training.finance.utils.AppContext;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

public class OperationListActivity extends BaseListActivity<Operation, BaseNodeListFragment, OperationSynchronizer> {

    private FloatingActionMenu menuCreateOperation;
    private FloatingActionButton fabIncome;
    private FloatingActionButton fabOutcome;
    private FloatingActionButton fabTransfer;
    private FloatingActionButton fabConvert;

    public OperationListActivity() {

        BaseNodeListFragment<Operation, OperationListAdapter, BaseNodeActionListener> fragment = new BaseNodeListFragment<>();

        init(fragment, R.layout.activity_operation_list, R.id.tlb_operation_list_actions, Initializer.getOperationSynchronizer());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fragment.setAdapter(new OperationListAdapter());

        setToolbarTitle(getResources().getString(R.string.operations));

        setSearchTitle(getResources().getString(R.string.operation_search_hint));

        setTransition(new TransitionSlide(this, TransitionSlide.Direction.RIGHT_LEFT));

        createDrawer(toolbar);

        menuCreateOperation.setMenuButtonShowAnimation(AnimationUtils.loadAnimation(this, R.anim.show_from_bottom));
        menuCreateOperation.setMenuButtonHideAnimation(AnimationUtils.loadAnimation(this, R.anim.hide_to_bottom));
    }

    private void runAddOperationActivity(Class activityClass, Operation operation) {
        Intent intent = new Intent(this, activityClass);
        intent.putExtra(AppContext.NODE_OBJECT, operation); // помещаем выбранный объект operation для передачи в активити
        intent.putExtra(AppContext.OPERATION_ACTION, AppContext.OPERATION_ADD);
        startActivityForResult(intent, AppContext.REQUEST_NODE_ADD, ActivityOptionsCompat.makeSceneTransitionAnimation(this).toBundle());
    }

    @Override
    protected void initSearchView(Menu menu) {

        super.initSearchView(menu);

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                searchView.clearFocus();

                if (query.trim().length() == 0) {
                    Toast.makeText(OperationListActivity.this, R.string.enter_search_string, Toast.LENGTH_SHORT).show();
                    return false;
                }

                search(query);

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                menuCreateOperation.hideMenu(true);
            }

            @Override
            public void onSearchViewClosed() {
                menuCreateOperation.showMenu(true);
                if (asyncSearch != null && asyncSearch.isSearched()) {
                    fragment.refreshList(Initializer.getOperationSynchronizer().getAll(), false);
                }
            }
        });
    }

    @Override
    protected void initListeners() {

        super.initListeners();

        menuCreateOperation.setOnMenuButtonClickListener(v -> menuCreateOperation.toggle(true));

        fabIncome.setOnClickListener(v -> runAddOperationActivity(EditIncomeOperationActivity.class, new IncomeOperation()));

        fabOutcome.setOnClickListener(v -> runAddOperationActivity(EditOutcomeOperationActivity.class, new OutcomeOperation()));

        fabTransfer.setOnClickListener(v -> runAddOperationActivity(EditTransferOperationActivity.class, new TransferOperation()));

        fabConvert.setOnClickListener(v -> runAddOperationActivity(EditConvertOperationActivity.class, new ConvertOperation()));
    }

    @Override
    protected void findComponents() {
        super.findComponents();
        menuCreateOperation = findViewById(R.id.fab_menu_create_operation);
        menuCreateOperation.setClosedOnTouchOutside(true);

        CoordinatorLayout.LayoutParams params = new CoordinatorLayout.LayoutParams(CoordinatorLayout.LayoutParams.MATCH_PARENT, CoordinatorLayout.LayoutParams.MATCH_PARENT);
        params.gravity = Gravity.CENTER_HORIZONTAL;

        menuCreateOperation.setLayoutParams(params);

        fabIncome = findViewById(R.id.fab_create_operation_income);
        fabOutcome = findViewById(R.id.fab_create_operation_outcome);
        fabTransfer = findViewById(R.id.fab_create_operation_transfer);
        fabConvert = findViewById(R.id.fab_create_operation_convert);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (menuCreateOperation.isOpened()) {
            menuCreateOperation.toggle(true);
        }
    }

    @Override
    public void onShowSnackBar() {
        super.onShowSnackBar();
        menuCreateOperation.hideMenu(true);
    }

    @Override
    public void onHideSnackBar() {
        super.onHideSnackBar();
        menuCreateOperation.showMenu(true);
    }

    @Override
    public void onScroll(RecyclerView recyclerView, int dx, int dy) {
        if (dy > 0) {// скорллинг вверх
            menuCreateOperation.hideMenu(true);
        } else if (dy <= 0) { // скорллинг вниз
            menuCreateOperation.showMenu(true);
        }
    }
}

