package com.example.training.finance.activities.edit_activities.edit_operation;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.training.finance.R;
import com.example.training.finance.activities.abstracts.BaseEditOperationActivity;
import com.example.training.finance.activities.list.StorageListActivity;
import com.example.training.finance.core.storage.exceptions.CurrencyException;
import com.example.training.finance.core.storage.impl.operations.TransferOperation;
import com.example.training.finance.core.storage.interfaces.Storage;
import com.example.training.finance.objects.LocalizedCurrency;
import com.example.training.finance.transitions.TransitionSlide;
import com.example.training.finance.utils.AppContext;
import com.example.training.finance.utils.ColorUtils;
import com.example.training.finance.utils.IconUtils;
import com.example.training.finance.utils.OperationTypeUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

public class EditTransferOperationActivity extends BaseEditOperationActivity<TransferOperation> {

    private static final String TAG = EditTransferOperationActivity.class.getName();
    protected TextView tvOperationStorageFrom;
    protected ViewGroup layoutOperationStorageFrom;
    protected ImageView icOperationStorageFrom;
    protected TextView tvOperationStorageTo;
    protected ViewGroup layoutOperationStorageTo;
    protected ImageView icOperationStorageTo;
    protected EditText etOperationAmount;
    protected Spinner spnCurrency;
    private List<LocalizedCurrency> currencyList = new ArrayList<>();
    private ArrayAdapter<LocalizedCurrency> сurrencyAdapter;

    public EditTransferOperationActivity() {
        super(R.layout.activity_edit_transfer_operation);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initComponents();

        tvOperationType.setText(OperationTypeUtils.transferType.toString());
        tvOperationType.setBackgroundColor(getBaseContext().getColor(ColorUtils.transferColor));

        if (actionType == AppContext.OPERATION_EDIT) {
            setValues();
        }

        layoutOperationStorageFrom.setOnClickListener(v -> {

            currentNodeSelect = tvOperationStorageFrom;

            Intent intent = new Intent(EditTransferOperationActivity.this, StorageListActivity.class);
            intent.putExtra(AppContext.TRANSITION_DIRECTION, TransitionSlide.Direction.RIGHT_LEFT);

            intent.putExtra(AppContext.LIST_VIEW_MODE, AppContext.SELECT_MODE);
            startActivityForResult(intent, REQUEST_SELECT_STORAGE_FROM, ActivityOptionsCompat.makeSceneTransitionAnimation(EditTransferOperationActivity.this).toBundle());
        });

        layoutOperationStorageTo.setOnClickListener(v -> {

            currentNodeSelect = tvOperationStorageTo;

            Intent intent = new Intent(EditTransferOperationActivity.this, StorageListActivity.class);
            intent.putExtra(AppContext.TRANSITION_DIRECTION, TransitionSlide.Direction.RIGHT_LEFT);

            intent.putExtra(AppContext.LIST_VIEW_MODE, AppContext.SELECT_MODE);
            startActivityForResult(intent, REQUEST_SELECT_STORAGE_TO, ActivityOptionsCompat.makeSceneTransitionAnimation(EditTransferOperationActivity.this).toBundle());
        });

        imgSave.setOnClickListener(v -> {

            if (!checkValues()) {
                return;
            }

            Currency selectedCurrency = ((LocalizedCurrency) spnCurrency.getSelectedItem()).getCurrency();

            if (!operation.getToStorage().getAvailableCurrencies().contains(selectedCurrency)) {
                try {
                    operation.getToStorage().addCurrency(selectedCurrency, BigDecimal.ZERO);
                } catch (CurrencyException e) {
                    Log.e(TAG, e.getMessage());
                }
            }

            operation.setFromAmount(convertString(etOperationAmount.getText().toString()));
            operation.setDescription(etOperationDesc.getText().toString());
            operation.setDateTime(calendar);
            operation.setFromCurrency(selectedCurrency);

            Intent intent = new Intent();
            intent.putExtra(AppContext.TRANSITION_DIRECTION, TransitionSlide.Direction.RIGHT_LEFT);
            intent.putExtra(AppContext.NODE_OBJECT, operation);
            setResult(RESULT_OK, intent);

            transition.finishWithTransition();
        });
    }

    private void setValues() {

        fillCurrencySpinner();

        etOperationAmount.setText(operation.getFromAmount().toString());

        icOperationStorageFrom.setImageDrawable(IconUtils.getIcon(operation.getFromStorage().getIconName()));
        icOperationStorageTo.setImageDrawable(IconUtils.getIcon(operation.getToStorage().getIconName()));

        tvOperationStorageTo.setText(operation.getToStorage().getName().toUpperCase());
        tvOperationStorageFrom.setText(operation.getFromStorage().getName().toUpperCase());

        tvOperationStorageTo.setTextColor(getResources().getColor(R.color.colorText, null));
        tvOperationStorageFrom.setTextColor(getResources().getColor(R.color.colorText, null));
    }

    private void initComponents() {
        icOperationStorageFrom = findViewById(R.id.ic_operation_from_storage_selected);
        icOperationStorageTo = findViewById(R.id.ic_operation_to_storage_selected);

        tvOperationStorageFrom = findViewById(R.id.tv_operation_from_storage_selected);
        tvOperationStorageTo = findViewById(R.id.tv_operation_to_storage_selected);
        etOperationAmount = findViewById(R.id.et_operation_amount_selected);

        layoutOperationStorageFrom = findViewById(R.id.layout_operation_from_storage);
        layoutOperationStorageTo = findViewById(R.id.layout_operation_to_storage);

        spnCurrency = findViewById(R.id.spn_currency);
        сurrencyAdapter = new ArrayAdapter<>(this, R.layout.spinner_currency_item, currencyList);
        spnCurrency.setAdapter(сurrencyAdapter);
    }

    private boolean checkValues() {

        if (etOperationAmount.getText().length() == 0) {
            Toast.makeText(EditTransferOperationActivity.this, R.string.enter_amount, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (operation.getFromStorage() == null) {
            Toast.makeText(EditTransferOperationActivity.this, R.string.select_storage_from, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (operation.getToStorage() == null) {
            Toast.makeText(EditTransferOperationActivity.this, R.string.select_storage_to, Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private void fillCurrencySpinner() {

        currencyList.clear();
        currencyList.addAll(getLocalizedCurrencyList(operation.getFromStorage().getAvailableCurrencies()));

        сurrencyAdapter.clear();
        сurrencyAdapter.addAll(getLocalizedCurrencyList(operation.getFromStorage().getAvailableCurrencies()));

        сurrencyAdapter.notifyDataSetChanged();

        spnCurrency.setSelection(currencyList.indexOf(operation.getFromCurrency()));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            Storage storage = (Storage) data.getSerializableExtra(AppContext.NODE_OBJECT);

            switch (requestCode) {
                case REQUEST_SELECT_STORAGE_FROM:

                    operation.setFromStorage(storage);
                    icOperationStorageFrom.setImageDrawable(IconUtils.getIcon(storage.getIconName()));
                    currentNodeSelect.setText(storage.getName().toUpperCase());
                    tvOperationStorageFrom.setTextColor(getResources().getColor(R.color.colorText, null));

                    updateCurrencyList(storage, currencyList, сurrencyAdapter, spnCurrency);

                    break;

                case REQUEST_SELECT_STORAGE_TO:
                    operation.setToStorage(storage);
                    icOperationStorageTo.setImageDrawable(IconUtils.getIcon(storage.getIconName()));
                    currentNodeSelect.setText(storage.getName().toUpperCase());
                    tvOperationStorageTo.setTextColor(getResources().getColor(R.color.colorText, null));

                    break;
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (operation.getFromStorage() != null) {
            updateCurrencyList(operation.getFromStorage(), currencyList, сurrencyAdapter, spnCurrency);
        }
    }
}
