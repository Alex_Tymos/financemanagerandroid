package com.example.training.finance.listeners;


import android.support.v7.widget.RecyclerView;

public interface BaseNodeActionListener<T> {

    void onSwipeOpen(T node);

    void onDelete(T node);

    void onAdd(T node);

    void onUpdate(T node);

    void onShowSnackBar();

    void onHideSnackBar();

    void onScroll(RecyclerView recyclerView, int dx, int dy);
}
