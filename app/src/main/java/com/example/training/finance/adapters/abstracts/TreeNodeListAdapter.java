package com.example.training.finance.adapters.abstracts;


import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.training.finance.R;
import com.example.training.finance.adapters.holders.TreeViewHolder;
import com.example.training.finance.core.storage.decorator.AbstractSynchronizer;
import com.example.training.finance.core.storage.interfaces.TreeNode;
import com.example.training.finance.listeners.TreeNodeActionListener;
import com.example.training.finance.utils.AppContext;
import com.malinskiy.superrecyclerview.swipe.SwipeLayout;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

public abstract class TreeNodeListAdapter<T extends TreeNode, VH extends TreeViewHolder> extends BaseNodeListAdapter<T, VH, TreeNodeActionListener<T>> {

    protected static final String TAG = TreeNodeListAdapter.class.getName();
    private static final int layoutId = R.layout.node_item;
    private int mode;

    public TreeNodeListAdapter(int mode, AbstractSynchronizer sync) {
        super(sync, layoutId);
        this.mode = mode;
    }

    public TreeNodeListAdapter(int mode, AbstractSynchronizer sync, List<T> initialList) {
        super(sync, layoutId, initialList);
        this.mode = mode;
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        super.onCreateViewHolder(parent, viewType);
        return null;
    }

    @Override
    public void onBindViewHolder(VH holder, final int position) {
        super.onBindViewHolder(holder, position);

        final T node = adapterList.get(position);

        if (node.doesHaveChildren()) {
            holder.tvChildCount.setText(String.valueOf(node.getChildren().size()));
            holder.tvChildCount.setBackgroundColor(ContextCompat.getColor(activityContext, R.color.colorGray));
            holder.imgSwipeDeleteNode.setVisibility(View.GONE);
        } else {
            holder.tvChildCount.setText("");
            holder.tvChildCount.setBackground(null);
            holder.imgSwipeDeleteNode.setVisibility(View.VISIBLE);
        }

        if (node.doesHaveChildren()) {
            holder.layoutShowChilds.setVisibility(View.VISIBLE);
            holder.layoutShowChilds.setOnClickListener(v -> refreshList((List<T>) node.getChildren(), node));
        } else {
            holder.layoutShowChilds.setVisibility(View.INVISIBLE);
        }
    }

    private void refreshList(List<T> list, T selectedNode) {
        refreshList(list, animatorChilds);
        listener.onShowChilds(selectedNode);
    }

    @Override
    protected void deleteWithSnackbar(T node, int position) {
        if (node.getRefCount() > 0) {
            closeSwipeLayouts();
            closeSnackBar();
            Toast.makeText(activityContext, R.string.has_operations, Toast.LENGTH_SHORT).show();
        } else {
            super.deleteWithSnackbar(node, position);
        }
    }

    protected void runAddChildActivity(T node) {
        closeSnackBar();
        openActivityOnClick(node, AppContext.REQUEST_NODE_ADD_CHILD);
    }

    public void insertChildNode(T node) {
        try {
            closeSwipeLayouts();
            sync.add(node);

            List<T> list = (List<T>) node.getParent().getChildren();

            if (comparator != null) {
                Collections.sort(list, comparator);
            }

            listener.onAdd(node);

            refreshList(list, (T) node.getParent());

            recyclerView.scrollToPosition(list.size() - 1);

        } catch (SQLException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    protected void initListeners(VH holder, final int position, final T node) {
        super.initListeners(holder, position, node);

        final SwipeLayout swipeLayout = holder.swipeLayout;

        swipeLayout.setDragEdge(SwipeLayout.DragEdge.Right);
        swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);

        holder.imgSwipeAddChildNode.setVisibility(View.VISIBLE);

        holder.imgSwipeAddChildNode.setOnClickListener(v -> runAddChildActivity(node));
    }

    @Override
    protected void nodeClickAction(int position, T node) {
        if (mode == AppContext.EDIT_MODE) {
            super.nodeClickAction(position, node);
        } else if (mode == AppContext.SELECT_MODE) {
            listener.returnNodeToOperationActivity(node);
        }
    }
}
