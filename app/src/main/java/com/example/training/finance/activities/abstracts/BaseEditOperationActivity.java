package com.example.training.finance.activities.abstracts;


import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.training.finance.R;
import com.example.training.finance.core.storage.interfaces.Operation;
import com.example.training.finance.core.storage.interfaces.Storage;
import com.example.training.finance.fragments.datetime.DatePickerFragment;
import com.example.training.finance.fragments.datetime.TimePickerFragment;
import com.example.training.finance.objects.LocalizedCurrency;
import com.example.training.finance.transitions.TransitionSlide;
import com.example.training.finance.utils.AppContext;
import com.example.training.finance.utils.CurrencyUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.List;

public abstract class BaseEditOperationActivity<T extends Operation> extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    protected static final int REQUEST_SELECT_SOURCE_TO = 1;
    protected static final int REQUEST_SELECT_SOURCE_FROM = 2;
    protected static final int REQUEST_SELECT_STORAGE_TO = 3;
    protected static final int REQUEST_SELECT_STORAGE_FROM = 4;
    protected Calendar calendar;
    protected Toolbar toolbar;
    protected TextView tvTitle;
    protected TextView tvOperationType;
    protected EditText etOperationDesc;
    protected TextView tvOperationDate;
    protected TextView tvOperationTime;
    protected ScrollView scrollView;
    protected TextView currentNodeSelect;
    protected ImageView imgSave;
    protected ImageView imgClose;
    protected int actionType;
    protected T operation;
    protected TransitionSlide transition;
    private TimePickerFragment timeFragment = new TimePickerFragment();
    private DatePickerFragment dateFragment = new DatePickerFragment();
    private int layoutId;


    public BaseEditOperationActivity(int layoutId) {
        this.layoutId = layoutId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(layoutId);
        findComponents();
        initListeners();

        actionType = getIntent().getIntExtra(AppContext.OPERATION_ACTION, -1);

        operation = (T) getIntent().getSerializableExtra(AppContext.NODE_OBJECT);

        setValues(actionType);
        changeTitle();

        transition = new TransitionSlide(this, TransitionSlide.Direction.RIGHT_LEFT);

        scrollView.smoothScrollTo(0, scrollView.getTop());
    }

    private void setValues(int action) {

        switch (action) {
            case AppContext.OPERATION_EDIT:
                calendar = operation.getDateTime();

                etOperationDesc.setText(operation.getDescription());
                break;

            case AppContext.OPERATION_ADD:
                calendar = Calendar.getInstance();
                break;
        }

        tvOperationDate.setText(formatDate(calendar));
        tvOperationTime.setText(formatTime(calendar));

        Bundle bundle = new Bundle();
        bundle.putSerializable(AppContext.DATE_CALENDAR, calendar);

        dateFragment.setArguments(bundle);
        timeFragment.setArguments(bundle);
    }

    private void findComponents() {
        toolbar = findViewById(R.id.tlb_edit_actions);
        setSupportActionBar(toolbar);

        tvOperationType = findViewById(R.id.tv_operation_type_selected);
        etOperationDesc = findViewById(R.id.et_operation_desc);
        tvOperationDate = findViewById(R.id.tv_operation_date);
        tvOperationTime = findViewById(R.id.tv_operation_time);
        scrollView = findViewById(R.id.scroll);
        imgClose = findViewById(R.id.img_node_close);
        imgSave = findViewById(R.id.img_node_save);
        tvTitle = findViewById(R.id.tv_node_name);
    }

    private void initListeners() {

        imgClose.setOnClickListener(v -> transition.finishWithTransition());

        tvOperationDate.setOnClickListener(v -> dateFragment.show(BaseEditOperationActivity.this.getFragmentManager(), AppContext.DATE_CALENDAR));

        tvOperationTime.setOnClickListener(v -> timeFragment.show(BaseEditOperationActivity.this.getFragmentManager(), AppContext.DATE_CALENDAR));
    }

    private void changeTitle() {
        if (operation.getId() > 0) {
            tvTitle.setText(R.string.editing);
        } else {
            tvTitle.setText(R.string.adding);
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        calendar.set(year, month, day);
        tvOperationDate.setText(formatDate(calendar));
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        tvOperationTime.setText(formatTime(calendar));
    }

    private String formatDate(Calendar calendar) {
        return DateUtils.formatDateTime(getBaseContext(), calendar.getTimeInMillis(), DateUtils.FORMAT_SHOW_DATE);
    }


    private String formatTime(Calendar calendar) {
        return DateUtils.formatDateTime(getBaseContext(), calendar.getTimeInMillis(), DateUtils.FORMAT_SHOW_TIME);
    }

    protected void updateCurrencyList(Storage storage, List<LocalizedCurrency> adapterList, ArrayAdapter<LocalizedCurrency> adapter, Spinner spinner) {

        LocalizedCurrency selectedCurrency = (LocalizedCurrency) spinner.getSelectedItem();

        adapterList.clear();
        adapterList.addAll(getLocalizedCurrencyList(storage.getAvailableCurrencies()));

        adapter.clear();
        adapter.addAll(getLocalizedCurrencyList(storage.getAvailableCurrencies()));

        adapter.notifyDataSetChanged();

        if (selectedCurrency != null) {
            if (storage.getAvailableCurrencies().contains(selectedCurrency)) {
                spinner.setSelection(adapterList.indexOf(selectedCurrency), true);
                return;
            }
        }

        if (selectedCurrency == null && storage.getAvailableCurrencies().contains(CurrencyUtils.defaultCurrency)) {
            spinner.setSelection(adapterList.indexOf(CurrencyUtils.defaultCurrency), true);
            return;
        }

        spinner.setSelection(0);
    }

    protected BigDecimal convertString(String value) {

        if (value.trim().length() != 0) {
            return new BigDecimal(value);
        }

        return BigDecimal.ZERO;
    }

    protected List<LocalizedCurrency> getLocalizedCurrencyList(List<Currency> list) {
        List<LocalizedCurrency> localizedList = new ArrayList<>();

        for (Currency c : list) {
            LocalizedCurrency lc = new LocalizedCurrency(c);
            localizedList.add(lc);
        }

        return localizedList;
    }
}

