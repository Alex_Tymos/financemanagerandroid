package com.example.training.finance.adapters.holders;


import android.view.View;
import android.widget.TextView;

import com.example.training.finance.R;

public class OperationViewHolder extends BaseViewHolder {

    public final TextView tvOperationSubtitle;
    public final TextView tvOperationAmount;
    public final TextView tvOperationTypeTag;
    public final TextView tvOperationConvertCurrency;
    public final TextView tvOperationCurrency;

    public OperationViewHolder(View view) {
        super(view);

        tvOperationSubtitle = view.findViewById(R.id.tv_operation_subtitle);
        tvOperationAmount = view.findViewById(R.id.tv_operation_amount);
        tvOperationTypeTag = view.findViewById(R.id.tv_operation_type_tag);
        tvOperationConvertCurrency = view.findViewById(R.id.tv_operation_amount_convert_currency);
        tvOperationCurrency = view.findViewById(R.id.tv_operation_currency);

    }
}
