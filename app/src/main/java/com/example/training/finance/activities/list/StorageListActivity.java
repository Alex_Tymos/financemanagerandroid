package com.example.training.finance.activities.list;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.training.finance.R;
import com.example.training.finance.activities.abstracts.TreeListActivity;
import com.example.training.finance.activities.edit_activities.EditStorageActivity;
import com.example.training.finance.adapters.StorageNodeAdapter;
import com.example.training.finance.core.storage.database.Initializer;
import com.example.training.finance.core.storage.decorator.StorageSynchronizer;
import com.example.training.finance.core.storage.exceptions.CurrencyException;
import com.example.training.finance.core.storage.impl.DefaultStorage;
import com.example.training.finance.core.storage.interfaces.Storage;
import com.example.training.finance.fragments.TreeNodeListFragment;
import com.example.training.finance.utils.AppContext;
import com.example.training.finance.utils.CurrencyUtils;
import com.example.training.finance.utils.LocaleUtils;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.math.BigDecimal;

public class StorageListActivity extends TreeListActivity<Storage, StorageSynchronizer> {

    protected static final String TAG = StorageListActivity.class.getName();
    private ViewGroup layoutTotalBalance;
    private TextView tvTotalBalance;
    private Storage oldSelectedParentNode;

    public StorageListActivity() {

        TreeNodeListFragment<Storage> fragment = new TreeNodeListFragment<>();

        init(fragment, R.layout.activity_storage_list, R.id.tlb_tree_list_actions, Initializer.getStorageSynchronizer());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        refreshTotalBalance();

        fragment.setAdapter(new StorageNodeAdapter(mode));

        setToolbarTitle(getResources().getString(R.string.storages));

        setSearchTitle(getResources().getString(R.string.storage_search_hint));

    }

    @Override
    protected void findComponents() {
        super.findComponents();
        layoutTotalBalance = findViewById(R.id.layout_total_balance);
        tvTotalBalance = findViewById(R.id.tv_total_balance);
    }

    @Override
    protected void showRootNodes() {
        super.showRootNodes();

        fragment.refreshList(Initializer.getStorageSynchronizer().getAll(), true);
    }

    protected void initListeners() {
        super.initListeners();

        fabCreateNode.setOnClickListener(v -> {

            Storage storage = new DefaultStorage();
            try {
                storage.addCurrency(CurrencyUtils.defaultCurrency, BigDecimal.ZERO);
            } catch (CurrencyException e) {
                Log.e(TAG, e.getMessage());
            }

            Intent intent = new Intent(StorageListActivity.this, EditStorageActivity.class);
            intent.putExtra(AppContext.NODE_OBJECT, storage);
            startActivityForResult(intent, AppContext.REQUEST_NODE_ADD, ActivityOptionsCompat.makeSceneTransitionAnimation(StorageListActivity.this).toBundle());

        });

    }

    public void refreshTotalBalance() {
        if (mode == AppContext.EDIT_MODE) {
            layoutTotalBalance.setVisibility(View.VISIBLE);
            tvTotalBalance.setText(getResources().getString(R.string.total_balance) + " ~ " + Initializer.getStorageSynchronizer().getTotalBalance(CurrencyUtils.defaultCurrency).setScale(0, BigDecimal.ROUND_UP).toString() + " " + CurrencyUtils.defaultCurrency.getSymbol(LocaleUtils.defaultLocale));
            return;
        }

        layoutTotalBalance.setVisibility(View.GONE);
    }

    @Override
    public void onAdd(Storage node) {
        super.onAdd(node);
        refreshTotalBalance();
    }

    @Override
    public void onDelete(Storage node) {
        super.onDelete(node);
        refreshTotalBalance();
    }

    @Override
    public void onUpdate(Storage node) {
        super.onUpdate(node);
        refreshTotalBalance();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        initSearchView(menu);

        return true;
    }

    @Override
    protected void initSearchView(Menu menu) {
        super.initSearchView(menu);

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                searchView.clearFocus();

                listBeforeSearch.clear();
                listBeforeSearch.addAll(fragment.getList());
                oldSelectedParentNode = parentNode;

                if (query.trim().length() == 0) {
                    Toast.makeText(StorageListActivity.this, R.string.enter_search_string, Toast.LENGTH_SHORT).show();
                    return false;
                }

                layoutTotalBalance.setVisibility(View.GONE);

                search(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                layoutTotalBalance.setVisibility(View.GONE);
                fabCreateNode.hide(true);
            }

            @Override
            public void onSearchViewClosed() {
                fabCreateNode.show(true);
                layoutTotalBalance.setVisibility(View.VISIBLE);

                if (asyncSearch != null && asyncSearch.isSearched()) {
                    parentNode = oldSelectedParentNode;
                    fragment.refreshList(listBeforeSearch, false);
                    asyncSearch = null;
                }
            }
        });
    }
}
