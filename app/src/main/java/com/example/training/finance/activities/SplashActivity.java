package com.example.training.finance.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.training.finance.R;
import com.example.training.finance.activities.list.OperationListActivity;
import com.example.training.finance.database.DbConnection;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        new Thread() {
            public void run() {

                DbConnection.initConnection(getApplicationContext());

                imitateLoading();

                Intent intent = new Intent(SplashActivity.this, OperationListActivity.class);
                startActivity(intent);
            }
        }.start();
    }

    private void imitateLoading() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

