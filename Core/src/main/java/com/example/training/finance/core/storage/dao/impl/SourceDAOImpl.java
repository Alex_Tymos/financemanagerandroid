package com.example.training.finance.core.storage.dao.impl;


import com.example.training.finance.core.storage.dao.interfaces.SourceDAO;
import com.example.training.finance.core.storage.database.SQLiteConnection;
import com.example.training.finance.core.storage.enums.OperationType;
import com.example.training.finance.core.storage.impl.DefaultSource;
import com.example.training.finance.core.storage.interfaces.Source;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class SourceDAOImpl implements SourceDAO {


    private List<Source> sourceList = new ArrayList<>();

    @Override
    public List<Source> getAll() {
        sourceList.clear();

        try (Statement stmt = SQLiteConnection.getConnection().createStatement();
             ResultSet rs = stmt.executeQuery("select * from " + SOURCE_TABLE + " order by parent_id")) {

            while (rs.next()) {
                DefaultSource source = new DefaultSource();
                source.setId(rs.getLong("id"));
                source.setName(rs.getString("name"));
                source.setParentId(rs.getLong("parent_id"));
                source.setIconName(rs.getString("icon_name"));
                source.setOperationType(OperationType.getType(rs.getInt("operation_type_id")));
                sourceList.add(source);
            }

            return sourceList;

        } catch (SQLException e) {
            Logger.getLogger(StorageDAOImpl.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

    public int getRefCount(Source source) {
        try (PreparedStatement stmt = SQLiteConnection.getConnection().prepareStatement("select ref_count from " + SOURCE_TABLE + " where id=?")) {

            stmt.setLong(1, source.getId());

            try (ResultSet rs = stmt.executeQuery();) {

                if (rs.next()) {
                    return rs.getInt("ref_count");
                }
            }

        } catch (SQLException e) {
            Logger.getLogger(SourceDAOImpl.class.getName()).log(Level.SEVERE, null, e);
        }

        return -1;
    }


    @Override
    public Source get(long id) {

        try (PreparedStatement stmt = SQLiteConnection.getConnection().prepareStatement("select * from " + SOURCE_TABLE + " where id=?")) {

            stmt.setLong(1, id);

            try (ResultSet rs = stmt.executeQuery();) {
                DefaultSource source = null;

                if (rs.next()) {
                    source = fillSource(rs);
                }

                return source;
            }

        } catch (SQLException e) {
            Logger.getLogger(SourceDAOImpl.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

    private DefaultSource fillSource(ResultSet rs) throws SQLException {

        DefaultSource source = new DefaultSource();
        source.setId(rs.getLong("id"));
        source.setName(rs.getString("name"));
        source.setParentId(rs.getLong("parent_id"));
        source.setIconName(rs.getString("icon_name"));
        source.setOperationType(OperationType.getType(rs.getInt("operation_type_id")));
        source.setRefCount(rs.getInt("ref_count"));

        return source;
    }


    @Override
    public boolean update(Source source) {

        try (PreparedStatement stmt = SQLiteConnection.getConnection().prepareStatement("update " + SOURCE_TABLE + " set name=?, icon_name=? where id=?")) {

            stmt.setString(1, source.getName());
            stmt.setString(2, source.getIconName());
            stmt.setLong(3, source.getId());

            if (stmt.executeUpdate() == 1) {
                return true;
            }

        } catch (SQLException e) {
            Logger.getLogger(SourceDAOImpl.class.getName()).log(Level.SEVERE, null, e);
        }

        return false;
    }


    @Override
    public boolean delete(Source source) throws SQLException {
        try (PreparedStatement stmt = SQLiteConnection.getConnection().prepareStatement("delete from " + SOURCE_TABLE + " where id=?")) {

            stmt.setLong(1, source.getId());

            if (stmt.executeUpdate() == 1) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean add(Source source) {
        try (PreparedStatement stmt = SQLiteConnection.getConnection().prepareStatement("insert into " + SOURCE_TABLE + "(name, parent_id, operation_type_id, icon_name) values(?,?,?,?)");
             Statement stmtId = SQLiteConnection.getConnection().createStatement()) {

            stmt.setString(1, source.getName());

            if (source.doesHaveParent()) {
                stmt.setLong(2, source.getParent().getId());
            } else {
                stmt.setNull(2, Types.BIGINT);
            }

            stmt.setLong(3, source.getOperationType().getId());
            stmt.setString(4, source.getIconName());

            if (stmt.executeUpdate() == 1) {
                try (ResultSet rs = stmtId.executeQuery("SELECT last_insert_rowid()")) {

                    if (rs.next()) {
                        source.setId(rs.getLong(1));
                        return true;
                    }
                }
            }

        } catch (SQLException e) {
            Logger.getLogger(SourceDAOImpl.class.getName()).log(Level.SEVERE, null, e);
        }

        return false;
    }

    @Override
    public List<Source> getList(OperationType operationType) {
        sourceList.clear();

        try (PreparedStatement stmt = SQLiteConnection.getConnection().prepareStatement("select * from " + SOURCE_TABLE + " where operation_type_id=?")) {

            stmt.setLong(1, operationType.getId());

            try (ResultSet rs = stmt.executeQuery()) {

                while (rs.next()) {
                    sourceList.add(fillSource(rs));
                }

                return sourceList;
            }

        } catch (SQLException e) {
            Logger.getLogger(SourceDAOImpl.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

    @Override
    public List<Source> search(String... params) {
        ArrayList<Source> list = new ArrayList<>();

        String searchStr = "%" + params[0] + "%";
        String sql = "select * from " + SOURCE_TABLE + " where name like ? ";

        if (params.length == 2) {
            sql += "and operation_type_id=?";
        }

        try (PreparedStatement stmt = SQLiteConnection.getConnection().prepareStatement(sql)) {


            stmt.setString(1, searchStr);

            if (params.length == 2) {
                stmt.setLong(2, Long.valueOf(params[1]));
            }

            try (ResultSet rs = stmt.executeQuery()) {

                while (rs.next()) {
                    list.add(fillSource(rs));
                }

                return list;
            }

        } catch (SQLException e) {
            Logger.getLogger(SourceDAOImpl.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }
}
