package com.example.training.finance.core.storage.utils;


import com.example.training.finance.core.storage.interfaces.TreeNode;

import java.util.List;

public class TreeConstructor<T extends TreeNode> {

    public boolean addToTree(long parentId, T newNode, List<T> treeList, List<T> plainList) {// plainList - где ищем компонент, treeList - дерево, куда добавляем
        if (parentId != 0) {
            for (T currentNode : plainList) {
                if (currentNode.getId() == parentId) {
                    currentNode.addChild(newNode);
                    return true;
                } else {
                    T node = recursiveSearch(parentId, currentNode);
                    if (node != null) {
                        node.addChild(newNode);
                        return true;
                    }
                }
            }
        }

        treeList.add(newNode);

        return false;
    }

    private T recursiveSearch(long parentId, T child) {
        for (T node : (List<T>) child.getChildren()) {
            if (node.getId() == parentId) {
                return node;
            } else if (node.doesHaveChildren()) {
                recursiveSearch(parentId, node);
            }
        }
        return null;
    }
}
