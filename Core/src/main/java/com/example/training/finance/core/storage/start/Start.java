package com.example.training.finance.core.storage.start;

import com.example.training.finance.core.storage.dao.impl.OperationDAOImpl;
import com.example.training.finance.core.storage.dao.impl.SourceDAOImpl;
import com.example.training.finance.core.storage.dao.impl.StorageDAOImpl;
import com.example.training.finance.core.storage.decorator.OperationSynchronizer;
import com.example.training.finance.core.storage.decorator.SourceSynchronizer;
import com.example.training.finance.core.storage.decorator.StorageSynchronizer;

public class Start {

    public static void main(String[] args) {

        StorageSynchronizer storageSynchronizer = new StorageSynchronizer(new StorageDAOImpl());
        SourceSynchronizer sourceSynchronizer = new SourceSynchronizer(new SourceDAOImpl());
        OperationSynchronizer operationSynchronizer = new OperationSynchronizer(new OperationDAOImpl(sourceSynchronizer.getIdentityMap(), storageSynchronizer.getIdentityMap()), sourceSynchronizer, storageSynchronizer);

//        Source parentSource = sourceSynchronizer.get(4);
//
//        DefaultSource s = new DefaultSource("test source 2");
//        s.setOperationType(OperationType.OUTCOME);
//        s.setParent(parentSource);
//        sourceSynchronizer.add(s);
//        System.out.println(" sourceSynchronizer = " + sourceSynchronizer.getAll());


//        try {
//            Storage storage = storageSynchronizer.get(15);
//            Source source = sourceSynchronizer.get(7);
//
//            OutcomeOperation operation = new OutcomeOperation();
//            operation.setFromCurrency(storage.getCurrency("USD"));
//            operation.setFromAmount(new BigDecimal(10));
//            operation.setFromStorage(storage);
//            operation.setToSource(source);
//            operation.setDateTime(Calendar.getInstance());
//            operation.setDescription("test desc");
//            operationSynchronizer.add(operation);
//            System.out.println("operationSynchronizer = " + operationSynchronizer);
//
//
//        } catch (CurrencyException e) {
//            e.printStackTrace();
//        }
//
//
    }


}