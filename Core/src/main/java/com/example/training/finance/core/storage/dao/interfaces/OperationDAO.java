package com.example.training.finance.core.storage.dao.interfaces;


import com.example.training.finance.core.storage.enums.OperationType;
import com.example.training.finance.core.storage.interfaces.Operation;

import java.util.List;

public interface OperationDAO extends CommonDAO<Operation> {

    String OPERATION_TABLE = "operation";

    List<Operation> getList(OperationType operationType);

}
