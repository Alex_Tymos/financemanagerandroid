package com.example.training.finance.core.storage.interfaces;


import java.util.List;

public interface TreeNode<T extends TreeNode> extends IconNode, RefNode {



    long getParentId();

    void addChild(T child);

    void removeChild(T child);

    List<T> getChildren();

    T getChild(long id);

    T getParent();

    void setParent(T parent);

    boolean doesHaveChildren();

    boolean doesHaveParent();

}
