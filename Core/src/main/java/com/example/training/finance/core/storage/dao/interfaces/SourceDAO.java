package com.example.training.finance.core.storage.dao.interfaces;


import com.example.training.finance.core.storage.enums.OperationType;
import com.example.training.finance.core.storage.interfaces.Source;

import java.util.List;

public interface SourceDAO extends CommonDAO<Source> {

    String SOURCE_TABLE = "source";

    List<Source> getList(OperationType operationType);

    int getRefCount(Source source);
}
