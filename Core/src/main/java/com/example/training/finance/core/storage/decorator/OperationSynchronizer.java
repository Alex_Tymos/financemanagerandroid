package com.example.training.finance.core.storage.decorator;


import com.example.training.finance.core.storage.dao.interfaces.OperationDAO;
import com.example.training.finance.core.storage.enums.OperationType;
import com.example.training.finance.core.storage.exceptions.CurrencyException;
import com.example.training.finance.core.storage.impl.operations.ConvertOperation;
import com.example.training.finance.core.storage.impl.operations.IncomeOperation;
import com.example.training.finance.core.storage.impl.operations.OutcomeOperation;
import com.example.training.finance.core.storage.impl.operations.TransferOperation;
import com.example.training.finance.core.storage.interfaces.Operation;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OperationSynchronizer extends AbstractSynchronizer<Operation> implements OperationDAO {

    private OperationDAO operationDAO;

    private List<Operation> operationList;
    private Map<OperationType, List<Operation>> operationMap = new EnumMap<>(OperationType.class);
    private Map<Long, Operation> identityMap = new HashMap<>();

    private SourceSynchronizer sourceSynchronizer;
    private StorageSynchronizer storageSynchronizer;

    public OperationSynchronizer(OperationDAO operationDAO, SourceSynchronizer sourceSynchronizer, StorageSynchronizer storageSynchronizer) {
        this.operationDAO = operationDAO;
        this.sourceSynchronizer = sourceSynchronizer;
        this.storageSynchronizer = storageSynchronizer;
        init();
    }

    private void init() {
        operationList = operationDAO.getAll();

        for (Operation s : operationList) {
            identityMap.put(s.getId(), s);
        }

        fillOperationMap();
    }

    private void fillOperationMap() {

//            operationMap.put(type, operationList.stream().filter(o -> o.getOperationType() == type).collect(Collectors.toList()));

        for (OperationType type : OperationType.values()) {
            ArrayList<Operation> incomeOperations = new ArrayList<>();
            ArrayList<Operation> outcomeOperations = new ArrayList<>();
            ArrayList<Operation> transferOperations = new ArrayList<>();
            ArrayList<Operation> convertOperations = new ArrayList<>();

            for (Operation o : operationList) {
                switch (o.getOperationType()) {
                    case INCOME: {
                        incomeOperations.add(o);
                        break;
                    }

                    case OUTCOME: {
                        outcomeOperations.add(o);
                        break;
                    }

                    case TRANSFER: {
                        transferOperations.add(o);
                        break;
                    }

                    case CONVERT: {
                        convertOperations.add(o);
                        break;
                    }
                }
            }

            operationMap.put(OperationType.INCOME, incomeOperations);
            operationMap.put(OperationType.OUTCOME, outcomeOperations);
            operationMap.put(OperationType.CONVERT, convertOperations);
            operationMap.put(OperationType.TRANSFER, transferOperations);
        }
    }

    @Override
    public List<Operation> getAll() {
        return operationList;
    }

    @Override
    public Operation get(long id) {
        return identityMap.get(id);
    }

    @Override
    public boolean update(Operation operation) throws SQLException {
        if (delete(operationDAO.get(operation.getId()))
                && add(operation)) {
            updateRefCount(operation);
            return true;
        }
        return false;
    }

    @Override
    public boolean delete(Operation operation) throws SQLException {
        // TODO добавить нужные Exceptions
        if (operationDAO.delete(operation) && revertBalance(operation)) {
            removeFromCollections(operation);
            updateRefCount(operation);
            return true;
        }
        return false;
    }

    private boolean revertBalance(Operation operation) throws SQLException {
        boolean updateAmountResult = false;

        try {

            switch (operation.getOperationType()) {
                case INCOME: {

                    IncomeOperation incomeOperation = (IncomeOperation) operation;

                    BigDecimal tmpAmount = storageSynchronizer.getIdentityMap().get(incomeOperation.getToStorage().getId()).getAmount(incomeOperation.getFromCurrency());

                    BigDecimal currentAmount = (tmpAmount == null) ? BigDecimal.ZERO : tmpAmount;
                    BigDecimal newAmount = currentAmount.subtract(incomeOperation.getFromAmount());

                    updateAmountResult = storageSynchronizer.updateAmount(incomeOperation.getToStorage(), incomeOperation.getFromCurrency(), newAmount);

                    break;

                }
                case OUTCOME: {

                    OutcomeOperation outcomeOperation = (OutcomeOperation) operation;

                    BigDecimal tmpAmount = storageSynchronizer.getIdentityMap().get(outcomeOperation.getFromStorage().getId()).getAmount(outcomeOperation.getFromCurrency());

                    BigDecimal currentAmount = (tmpAmount == null) ? BigDecimal.ZERO : tmpAmount;
                    BigDecimal newAmount = currentAmount.add(outcomeOperation.getFromAmount());

                    updateAmountResult = storageSynchronizer.updateAmount(outcomeOperation.getFromStorage(), outcomeOperation.getFromCurrency(), newAmount);

                    break;
                }

                case TRANSFER: {

                    TransferOperation trasnferOperation = (TransferOperation) operation;

                    BigDecimal tmpAmount = storageSynchronizer.getIdentityMap().get(trasnferOperation.getFromStorage().getId()).getAmount(trasnferOperation.getFromCurrency());

                    BigDecimal currentAmountFromStorage = (tmpAmount == null) ? BigDecimal.ZERO : tmpAmount;
                    BigDecimal newAmountFromStorage = currentAmountFromStorage.add(trasnferOperation.getFromAmount());

                    tmpAmount = storageSynchronizer.getIdentityMap().get(trasnferOperation.getToStorage().getId()).getAmount(trasnferOperation.getFromCurrency());

                    BigDecimal currentAmountToStorage = (tmpAmount == null) ? BigDecimal.ZERO : tmpAmount;
                    BigDecimal newAmountToStorage = currentAmountToStorage.subtract(trasnferOperation.getFromAmount());

                    updateAmountResult = storageSynchronizer.updateAmount(trasnferOperation.getFromStorage(), trasnferOperation.getFromCurrency(), newAmountFromStorage) &&
                            storageSynchronizer.updateAmount(trasnferOperation.getToStorage(), trasnferOperation.getFromCurrency(), newAmountToStorage);

                    break;
                }

                case CONVERT: {
                    ConvertOperation convertOperation = (ConvertOperation) operation;

                    BigDecimal tmpAmount = storageSynchronizer.getIdentityMap().get(convertOperation.getFromStorage().getId()).getAmount(convertOperation.getFromCurrency());

                    BigDecimal currentAmountFromStorage = (tmpAmount == null) ? BigDecimal.ZERO : tmpAmount;
                    BigDecimal newAmountFromStorage = currentAmountFromStorage.add(convertOperation.getFromAmount());

                    tmpAmount = storageSynchronizer.getIdentityMap().get(convertOperation.getToStorage().getId()).getAmount(convertOperation.getToCurrency());

                    BigDecimal currentAmountToStorage = (tmpAmount == null) ? BigDecimal.ZERO : tmpAmount;
                    BigDecimal newAmountToStorage = currentAmountToStorage.subtract(convertOperation.getToAmount());


                    updateAmountResult = storageSynchronizer.updateAmount(convertOperation.getFromStorage(), convertOperation.getFromCurrency(), newAmountFromStorage) &&
                            storageSynchronizer.updateAmount(convertOperation.getToStorage(), convertOperation.getToCurrency(), newAmountToStorage);

                    break;
                }
            }

        } catch (CurrencyException e) {
            e.printStackTrace();
        }

        if (!updateAmountResult) {
            delete(operation);
            return false;
        }

        return true;
    }

    private void removeFromCollections(Operation operation) {
        operationList.remove(operation);
        identityMap.remove(operation.getId());
        operationDAO.getList(operation.getOperationType()).remove(operation);
    }

    @Override
    public boolean add(Operation operation) throws SQLException {
        if (operationDAO.add(operation)) {
            addToCollections(operation);

            if (updateBalance(operation)) {

                updateRefCount(operation);

                return true;
            }

        }
        return false;
    }

    private void updateRefCount(Operation operation) {

        switch (operation.getOperationType()) {
            case OUTCOME:
                OutcomeOperation outcomeOperation = (OutcomeOperation) operation;
                storageSynchronizer.getIdentityMap().get(outcomeOperation.getFromStorage().getId()).setRefCount(storageSynchronizer.getRefCount(outcomeOperation.getFromStorage()));
                sourceSynchronizer.getIdentityMap().get(outcomeOperation.getToSource().getId()).setRefCount(sourceSynchronizer.getRefCount(outcomeOperation.getToSource()));

                break;

            case INCOME:
                IncomeOperation incomeOperation = (IncomeOperation) operation;
                storageSynchronizer.getIdentityMap().get(incomeOperation.getToStorage().getId()).setRefCount(storageSynchronizer.getRefCount(incomeOperation.getToStorage()));
                sourceSynchronizer.getIdentityMap().get(incomeOperation.getFromSource().getId()).setRefCount(sourceSynchronizer.getRefCount(incomeOperation.getFromSource()));

                break;

            case TRANSFER:
                TransferOperation transferOperation = (TransferOperation) operation;
                storageSynchronizer.getIdentityMap().get(transferOperation.getToStorage().getId()).setRefCount(storageSynchronizer.getRefCount(transferOperation.getToStorage()));
                storageSynchronizer.getIdentityMap().get(transferOperation.getFromStorage().getId()).setRefCount(storageSynchronizer.getRefCount(transferOperation.getFromStorage()));

                break;

            case CONVERT:
                ConvertOperation convertOperation = (ConvertOperation) operation;
                storageSynchronizer.getIdentityMap().get(convertOperation.getToStorage().getId()).setRefCount(storageSynchronizer.getRefCount(convertOperation.getToStorage()));
                storageSynchronizer.getIdentityMap().get(convertOperation.getFromStorage().getId()).setRefCount(storageSynchronizer.getRefCount(convertOperation.getFromStorage()));

                break;
        }
    }

    private boolean updateBalance(Operation operation) throws SQLException {
        boolean updateAmountResult = false;

        try {

            switch (operation.getOperationType()) {
                case INCOME: {

                    IncomeOperation incomeOperation = (IncomeOperation) operation;

                    BigDecimal tmpAmount = incomeOperation.getToStorage().getAmount(incomeOperation.getFromCurrency());

                    BigDecimal currentAmount = tmpAmount != null ? tmpAmount : BigDecimal.ZERO;
                    BigDecimal newAmount = currentAmount.add(incomeOperation.getFromAmount());

                    updateAmountResult = storageSynchronizer.updateAmount(incomeOperation.getToStorage(), incomeOperation.getFromCurrency(), newAmount);

                    break;
                }

                case OUTCOME: {

                    OutcomeOperation outcomeOperation = (OutcomeOperation) operation;

                    BigDecimal tmpAmount = outcomeOperation.getFromStorage().getAmount(outcomeOperation.getFromCurrency());

                    BigDecimal currentAmount = tmpAmount != null ? tmpAmount : BigDecimal.ZERO;
                    BigDecimal newAmount = currentAmount.subtract(outcomeOperation.getFromAmount());

                    updateAmountResult = storageSynchronizer.updateAmount(outcomeOperation.getFromStorage(), outcomeOperation.getFromCurrency(), newAmount);

                    break;
                }

                case TRANSFER: {

                    TransferOperation trasnferOperation = (TransferOperation) operation;

                    BigDecimal tmpAmount = trasnferOperation.getFromStorage().getAmount(trasnferOperation.getFromCurrency());

                    BigDecimal currentAmountFromStorage = tmpAmount != null ? tmpAmount : BigDecimal.ZERO;
                    BigDecimal newAmountFromStorage = currentAmountFromStorage.subtract(trasnferOperation.getFromAmount());

                    tmpAmount = trasnferOperation.getToStorage().getAmount(trasnferOperation.getFromCurrency());
                    BigDecimal currentAmountToStorage = tmpAmount != null ? tmpAmount : BigDecimal.ZERO;
                    BigDecimal newAmountToStorage = currentAmountToStorage.add(trasnferOperation.getFromAmount());

                    updateAmountResult = storageSynchronizer.updateAmount(trasnferOperation.getFromStorage(), trasnferOperation.getFromCurrency(), newAmountFromStorage) &&
                            storageSynchronizer.updateAmount(trasnferOperation.getToStorage(), trasnferOperation.getFromCurrency(), newAmountToStorage);

                    break;

                }

                case CONVERT: {
                    ConvertOperation convertOperation = (ConvertOperation) operation;

                    BigDecimal tmpAmount = convertOperation.getFromStorage().getAmount(convertOperation.getFromCurrency());
                    BigDecimal currentAmountFromStorage = (tmpAmount != null) ? tmpAmount : BigDecimal.ZERO;
                    BigDecimal newAmountFromStorage = currentAmountFromStorage.subtract(convertOperation.getFromAmount());

                    tmpAmount = convertOperation.getToStorage().getAmount(convertOperation.getToCurrency());
                    BigDecimal currentAmountToStorage = (tmpAmount != null) ? tmpAmount : BigDecimal.ZERO;
                    BigDecimal newAmountToStorage = currentAmountToStorage.add(convertOperation.getToAmount());

                    updateAmountResult = storageSynchronizer.updateAmount(convertOperation.getFromStorage(), convertOperation.getFromCurrency(), newAmountFromStorage) &&
                            storageSynchronizer.updateAmount(convertOperation.getToStorage(), convertOperation.getToCurrency(), newAmountToStorage);

                    break;
                }
            }

        } catch (CurrencyException e) {
            e.printStackTrace();
        }

        if (!updateAmountResult) {
            delete(operation);
            return false;
        }
        return true;
    }


    private void addToCollections(Operation operation) {
        operationList.add(operation);
        identityMap.put(operation.getId(), operation);
        operationMap.get(operation.getOperationType()).add(operation);
    }


    @Override
    public List<Operation> getList(OperationType operationType) {
        return operationMap.get(operationType);
    }

    @Override
    public List<Operation> search(String... params) {
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        return operationDAO.search(params);
    }
}



