package com.example.training.finance.core.storage.impl.operations;


import com.example.training.finance.core.storage.abstracts.AbstractOperation;
import com.example.training.finance.core.storage.enums.OperationType;
import com.example.training.finance.core.storage.interfaces.Source;
import com.example.training.finance.core.storage.interfaces.Storage;

import java.math.BigDecimal;
import java.util.Currency;

public class IncomeOperation extends AbstractOperation {

    private Source fromSource;
    private Storage toStorage;
    private BigDecimal fromAmount;
    private Currency fromCurrency;

    public IncomeOperation() {
        super(OperationType.INCOME);
    }

    public Source getFromSource() {
        return fromSource;
    }

    public void setFromSource(Source fromSource) {
        this.fromSource = fromSource;
    }

    public Storage getToStorage() {
        return toStorage;
    }

    public void setToStorage(Storage toStorage) {
        this.toStorage = toStorage;
    }

    public BigDecimal getFromAmount() {
        return fromAmount;
    }

    public void setFromAmount(BigDecimal fromAmount) {
        this.fromAmount = fromAmount;
    }

    public Currency getFromCurrency() {
        return fromCurrency;
    }

    public void setFromCurrency(Currency currency) {
        this.fromCurrency = fromCurrency;
    }
}
