package com.example.training.finance.core.storage.exceptions;


public class AmountException extends Exception {

    public AmountException() {
        super();
    }

    public AmountException(String message) {
        super(message);
    }

    public AmountException(String message, Throwable cause) {
        super(message, cause);
    }

    public AmountException(Throwable cause) {
        super(cause);
    }

    public AmountException(String message, Throwable cause, boolean enableSupperession, boolean writableStackTrace) {
        super(message, cause, enableSupperession, writableStackTrace);
    }
}
