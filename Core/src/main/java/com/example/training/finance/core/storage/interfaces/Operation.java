package com.example.training.finance.core.storage.interfaces;


import com.example.training.finance.core.storage.enums.OperationType;

import java.util.Calendar;

public interface Operation extends IconNode {

    long getId();

    void setId(long id);

    OperationType getOperationType();

    Calendar getDateTime();

    String getDescription();
}
