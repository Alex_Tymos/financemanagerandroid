package com.example.training.finance.core.storage.utils;


import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;

public class RateUtils {

    public static Map<String, Map<String, BigDecimal>> rateMap = new HashMap<>();

    static {

        Map<String, BigDecimal> convertMap = new HashMap<>();
        convertMap.put(Currency.getInstance("USD").getCurrencyCode(), new BigDecimal(27));
        convertMap.put(Currency.getInstance("RUB").getCurrencyCode(), new BigDecimal(0.43));

        // ключ - валюта, в которую переводим
        rateMap.put(Currency.getInstance("UAH").getCurrencyCode(), convertMap);

    }

    public static BigDecimal getRate(Currency currencyFrom, Currency currencyTo) {
        return rateMap.get(currencyFrom.getCurrencyCode()).get(currencyTo.getCurrencyCode());
    }

    public static void setRateMap(Map<String, Map<String, BigDecimal>> rateMap) {
        RateUtils.rateMap = rateMap;
    }
}


