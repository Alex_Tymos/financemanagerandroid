package com.example.training.finance.core.storage.database;


import com.example.training.finance.core.storage.dao.impl.OperationDAOImpl;
import com.example.training.finance.core.storage.dao.impl.SourceDAOImpl;
import com.example.training.finance.core.storage.dao.impl.StorageDAOImpl;
import com.example.training.finance.core.storage.decorator.OperationSynchronizer;
import com.example.training.finance.core.storage.decorator.SourceSynchronizer;
import com.example.training.finance.core.storage.decorator.StorageSynchronizer;

public class Initializer {

    private static OperationSynchronizer operationSynchronizer;
    private static SourceSynchronizer sourceSynchronizer;
    private static StorageSynchronizer storageSynchronizer;


    public static OperationSynchronizer getOperationSynchronizer() {
        return operationSynchronizer;
    }


    public static SourceSynchronizer getSourceSynchronizer() {
        return sourceSynchronizer;
    }


    public static StorageSynchronizer getStorageSynchronizer() {
        return storageSynchronizer;
    }


    public static void load(String driverName, String url) {

        SQLiteConnection.init(driverName, url);

        sourceSynchronizer = new SourceSynchronizer(new SourceDAOImpl());
        storageSynchronizer = new StorageSynchronizer(new StorageDAOImpl());
        operationSynchronizer = new OperationSynchronizer(new OperationDAOImpl(sourceSynchronizer.getIdentityMap(), storageSynchronizer.getIdentityMap()), sourceSynchronizer, storageSynchronizer);
    }

}


