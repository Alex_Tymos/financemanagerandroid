package com.example.training.finance.core.storage.enums;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum OperationType {

    INCOME(1),
    OUTCOME(2),
    TRANSFER(3),
    CONVERT(4);

    private static Map<Integer, OperationType> map = new HashMap<>();
    private static List<OperationType> list = new ArrayList<>();

    static {
        for (OperationType operationType : OperationType.values()) {
            map.put(operationType.getId(), operationType);
            list.add(operationType);
        }
    }

    private Integer id;

    OperationType(Integer id) {
        this.id = id;
    }

    public static OperationType getType(int id) {
        return map.get(id);
    }

    public static List<OperationType> getList() {
        return list;
    }

    public Integer getId() {
        return id;
    }
}

