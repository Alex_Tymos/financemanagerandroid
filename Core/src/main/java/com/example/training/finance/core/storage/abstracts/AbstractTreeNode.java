package com.example.training.finance.core.storage.abstracts;


import com.example.training.finance.core.storage.interfaces.TreeNode;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractTreeNode<T extends TreeNode> implements TreeNode<T> {

    protected String name;
    private long id = -1;
    private List<T> children = new ArrayList<>();
    private T parent;
    private long parentId;
    private String iconName;
    private int refCount;

    public AbstractTreeNode() {
    }

    public AbstractTreeNode(long id) {
        this.id = id;
    }

    public AbstractTreeNode(String name) {
        this.name = name;
    }

    public AbstractTreeNode(List<T> children) {
        this.children = children;
    }

    public AbstractTreeNode(long id, List<T> children, T parent, String name) {
        this.id = id;
        this.children = children;
        this.parent = parent;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getIconName() {
        return iconName;
    }

    @Override
    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    @Override
    public void addChild(T child) {
        child.setParent(this);
        children.add(child);
    }

    @Override
    public T getParent() {
        return parent;
    }

    @Override
    public void setParent(T parent) {
        this.parent = parent;
    }

    @Override
    public void removeChild(T child) {
        children.remove(child);
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public T getChild(long id) {
        for (T child : children) {
            if (child.getId() == id) {
                return child;
            }
        }
        return null;
    }

    @Override
    public boolean doesHaveChildren() {
        return !children.isEmpty();
    }

    @Override
    public boolean doesHaveParent() {
        return parent != null;
    }

    @Override
    public List<T> getChildren() {
        return children;
    }

    @Override
    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    @Override
    public int getRefCount() {
        return refCount;
    }

    @Override
    public void setRefCount(int refCount) {
        this.refCount = refCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractTreeNode that = (AbstractTreeNode) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return name;
    }
}
