package com.example.training.finance.core.storage.decorator;


import com.example.training.finance.core.storage.dao.interfaces.StorageDAO;
import com.example.training.finance.core.storage.exceptions.AmountException;
import com.example.training.finance.core.storage.exceptions.CurrencyException;
import com.example.training.finance.core.storage.interfaces.Storage;
import com.example.training.finance.core.storage.utils.TreeConstructor;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StorageSynchronizer extends AbstractSynchronizer<Storage> implements StorageDAO {

    private TreeConstructor<Storage> treeConstructor = new TreeConstructor();


    private List<Storage> treeList = new ArrayList<>();
    private Map<Long, Storage> identityMap = new HashMap<>();

    private StorageDAO storageDAO;

    public StorageSynchronizer(StorageDAO storageDAO) {
        this.storageDAO = storageDAO;
        init();
    }

    private void init() {
        List<Storage> storageList = storageDAO.getAll();

        for (Storage s : storageList) {
            identityMap.put(s.getId(), s);
            treeConstructor.addToTree(s.getParentId(), s, treeList, storageList);
        }
    }

    @Override
    public List<Storage> getAll() {
        return treeList;
    }

    @Override
    public Storage get(long id) {
        return identityMap.get(id);
    }

    @Override
    public boolean update(Storage storage) throws SQLException {
        if (storageDAO.update(storage)) {

            Storage s = identityMap.get(storage.getId());

            s.setName(storage.getName());
            s.setIconName(storage.getIconName());
            s.getCurrencyAmounts().clear();
            s.getAvailableCurrencies().clear();
            s.getCurrencyAmounts().putAll(storage.getCurrencyAmounts());
            s.getAvailableCurrencies().addAll(storage.getAvailableCurrencies());

            return true;
        }
        return false;
    }

    @Override
    public boolean delete(Storage storage) throws SQLException {
        if (storageDAO.delete(storage)) {
            removeFromCollections(storage);

            return true;
        }
        return false;
    }

    @Override
    public boolean add(Storage storage) throws SQLException {

        if (storageDAO.add(storage)) {
            addToCollections(storage);
            return true;
        } else {

        }

        return false;
    }

    private void addToCollections(Storage storage) {
        identityMap.put(storage.getId(), storage);

        if (storage.doesHaveParent()) {
            Storage parent = identityMap.get(storage.getParent().getId());
            if (!parent.getChildren().contains(storage)) {
                parent.addChild(storage);
            }
        } else {
            treeList.add(storage);
        }
    }

    private void removeFromCollections(Storage storage) {
        identityMap.remove(storage.getId());
        if (storage == null) {
            return;
        }

        if (storage.doesHaveParent()) {
            storage.getParent().removeChild(storage);
        } else {
            treeList.remove(storage);
        }
    }

    @Override
    public boolean addCurrency(Storage storage, Currency currency, BigDecimal initAmount) throws CurrencyException {
        if (storageDAO.addCurrency(storage, currency, initAmount)) {
            identityMap.get(storage.getId()).addCurrency(currency, initAmount);
            return true;
        }

        return false;
    }

    @Override
    public boolean deleteCurrency(Storage storage, Currency currency) throws CurrencyException {
        if (storageDAO.deleteCurrency(storage, currency)) {
            identityMap.get(storage.getId()).deleteCurrency(currency);
            return true;
        }

        return false;
    }

    @Override
    public boolean updateAmount(Storage storage, Currency currency, BigDecimal amount) {
        if (storageDAO.updateAmount(storage, currency, amount)) {
            try {
                identityMap.get(storage.getId()).updateAmount(amount, currency);
            } catch (CurrencyException e) {
                e.printStackTrace();
            } catch (AmountException e) {
                e.printStackTrace();
            }
            return true;
        }

        return false;
    }

    @Override
    public int getRefCount(Storage storage) {
        return storageDAO.getRefCount(storage);
    }

    public BigDecimal getTotalBalance(Currency currency) {
        BigDecimal sum = BigDecimal.ZERO;

        for (Storage s : treeList) {
            try {
                sum = sum.add(s.getApproxAmount(currency));
            } catch (CurrencyException e) {
                e.printStackTrace();
            }
        }

        return sum;
    }


    public Map<Long, Storage> getIdentityMap() {
        return identityMap;
    }


    public StorageDAO getStorageDAO() {
        return storageDAO;
    }

    @Override
    public List<Storage> search(String... params) {
        return storageDAO.search(params);
    }

}
