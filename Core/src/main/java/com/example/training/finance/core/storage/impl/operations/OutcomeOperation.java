package com.example.training.finance.core.storage.impl.operations;

import com.example.training.finance.core.storage.abstracts.AbstractOperation;
import com.example.training.finance.core.storage.enums.OperationType;
import com.example.training.finance.core.storage.interfaces.Source;
import com.example.training.finance.core.storage.interfaces.Storage;

import java.math.BigDecimal;
import java.util.Currency;


public class OutcomeOperation extends AbstractOperation {

    private Storage fromStorage;
    private Source toSource;
    private BigDecimal fromAmount;
    private Currency fromCurrency;


    public OutcomeOperation() {
        super(OperationType.OUTCOME);
    }

    public Storage getFromStorage() {
        return fromStorage;
    }

    public void setFromStorage(Storage fromStorage) {
        this.fromStorage = fromStorage;
    }

    public Source getToSource() {
        return toSource;
    }

    public void setToSource(Source toSource) {
        this.toSource = toSource;
    }

    public BigDecimal getFromAmount() {
        return fromAmount;
    }

    public void setFromAmount(BigDecimal fromAmount) {
        this.fromAmount = fromAmount;
    }

    public Currency getFromCurrency() {
        return fromCurrency;
    }

    public void setFromCurrency(Currency fromCurrency) {
        this.fromCurrency = fromCurrency;
    }

}
