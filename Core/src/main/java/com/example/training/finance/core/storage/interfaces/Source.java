package com.example.training.finance.core.storage.interfaces;


import com.example.training.finance.core.storage.enums.OperationType;

public interface Source<T extends Source> extends TreeNode<T> {

    OperationType getOperationType();

    void setOperationType(OperationType type);


}
