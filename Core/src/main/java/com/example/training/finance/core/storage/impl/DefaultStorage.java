package com.example.training.finance.core.storage.impl;


import com.example.training.finance.core.storage.abstracts.AbstractTreeNode;
import com.example.training.finance.core.storage.exceptions.AmountException;
import com.example.training.finance.core.storage.exceptions.CurrencyException;
import com.example.training.finance.core.storage.interfaces.Storage;
import com.example.training.finance.core.storage.utils.RateUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DefaultStorage extends AbstractTreeNode<DefaultStorage> implements Storage<DefaultStorage> {


    private Map<Currency, BigDecimal> currencyAmounts = new HashMap<>();
//    private List<Currency> currencyList = new ArrayList<>();

    public DefaultStorage() {
    }

    public DefaultStorage(String name) {
        super(name);
    }

//    public DefaultStorage(String name, long id) {
//        super(name, id);
//    }

    public DefaultStorage(List<Currency> currencyList, Map<Currency, BigDecimal> currencyAmounts, String name) {
        super(name);
        this.currencyAmounts = currencyAmounts;
    }

    public DefaultStorage(Map<Currency, BigDecimal> currencyAmounts) {
        this.currencyAmounts = currencyAmounts;
    }

    @Override
    public Map<Currency, BigDecimal> getCurrencyAmounts() {
        return currencyAmounts;
    }

    @Override
    public List<Currency> getAvailableCurrencies() {
        return new ArrayList(currencyAmounts.keySet());
    }

    @Override
    public BigDecimal getAmount(Currency currency) throws CurrencyException {
//        checkCurrencyExist(currency);
        return currencyAmounts.get(currency);
    }

    @Override
    public BigDecimal getApproxAmount(Currency targetCurrency) {

        BigDecimal sum = BigDecimal.ZERO;

        for (Currency c : getAvailableCurrencies()) {

            try {
                if (c.equals(targetCurrency)) {
                    sum = sum.add(getAmount(c));
                } else {
                    sum = sum.add(RateUtils.getRate(targetCurrency, c).multiply(getAmount(c)));
                }
            } catch (CurrencyException e) {
                e.printStackTrace();
            }
        }

        for (Storage storage : getChildren()) {
            try {
                sum = sum.add(storage.getApproxAmount(targetCurrency));
            } catch (CurrencyException e) {
                e.printStackTrace();
            }
        }

        return sum;
    }


    private void checkCurrencyExist(Currency currency) throws CurrencyException {
        if (!currencyAmounts.containsKey(currency)) {
            throw new CurrencyException("Currency" + currency + "does not exist");
        }
    }

    @Override
    public void updateAmount(BigDecimal amount, Currency currency) throws CurrencyException, AmountException {

        currencyAmounts.put(currency, amount);
    }

//    private void checkAmount(BigDecimal amount) throws AmountException {
//        if (amount.compareTo(BigDecimal.ZERO) < 0) {
//            throw new AmountException("Amount can`t be <0");
//        }
//    }

    @Override
    public Currency getCurrency(String code) throws CurrencyException {
        for (Currency currency : getAvailableCurrencies()) {
            if (currency.getCurrencyCode().equals(code)) {
                return currency;
            }
        }

        throw new CurrencyException("Currency (code=" + code + ") not exist in storage");
    }

    @Override
    public void addCurrency(Currency currency, BigDecimal initAmount) throws CurrencyException {

        if (currencyAmounts.containsKey(currency)) {
            throw new CurrencyException("Currency already exists");
        }

        currencyAmounts.put(currency, initAmount);
    }

    @Override
    public void deleteCurrency(Currency currency) throws CurrencyException {
        checkCurrencyExist(currency);

        currencyAmounts.remove(currency);
    }

    @Override
    public void deleteAllCurrencies() {
        currencyAmounts.clear();
    }

}
