package com.example.training.finance.core.storage.decorator;


import com.example.training.finance.core.storage.dao.interfaces.SourceDAO;
import com.example.training.finance.core.storage.enums.OperationType;
import com.example.training.finance.core.storage.interfaces.Source;
import com.example.training.finance.core.storage.utils.TreeConstructor;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SourceSynchronizer extends AbstractSynchronizer<Source> implements SourceDAO {

    private TreeConstructor<Source> treeConstructor = new TreeConstructor();

    private List<Source> treeList = new ArrayList<>();
    private Map<OperationType, List<Source>> sourceMap = new EnumMap<>(OperationType.class);
    private Map<Long, Source> identityMap = new HashMap<>();
    private String[] sourceArray;

    private SourceDAO sourceDAO;

    public SourceSynchronizer(SourceDAO sourceDAO) {
        this.sourceDAO = sourceDAO;
        init();
    }

    public void init() {
        List<Source> sourceList = sourceDAO.getAll();

        for (Source s : sourceList) {
            identityMap.put(s.getId(), s);
            treeConstructor.addToTree(s.getParentId(), s, treeList, sourceList);
        }

        fillSourceMap(treeList);
    }

    private void fillSourceMap(List<Source> list) {
//        for (OperationType type : OperationType.values()) {
//            sourceMap.put(type, list.stream().filter(s -> s.getOperationType() == type).collect(Collectors.toList()));

        for (OperationType type : OperationType.values()) {
            ArrayList<Source> incomeSources = new ArrayList<>();
            ArrayList<Source> outcomeSources = new ArrayList<>();
            ArrayList<Source> transferSources = new ArrayList<>();
            ArrayList<Source> convertSources = new ArrayList<>();

            // проход по коллекции только один раз
            for (Source o : list) {
                switch (o.getOperationType()) {
                    case INCOME: {
                        incomeSources.add(o);
                        break;
                    }

                    case OUTCOME: {
                        outcomeSources.add(o);
                        break;
                    }

                    case TRANSFER: {
                        transferSources.add(o);
                        break;
                    }

                    case CONVERT: {
                        convertSources.add(o);
                        break;
                    }
                }
            }

            sourceMap.put(OperationType.INCOME, incomeSources);
            sourceMap.put(OperationType.OUTCOME, outcomeSources);
            sourceMap.put(OperationType.CONVERT, convertSources);
            sourceMap.put(OperationType.TRANSFER, transferSources);
        }
    }

    @Override
    public int getRefCount(Source source) {
        return sourceDAO.getRefCount(source);
    }

    @Override
    public List<Source> getAll() {
        return treeList;
    }

    @Override
    public Source get(long id) {
        return identityMap.get(id);
    }

    @Override
    public boolean update(Source source) throws SQLException {
        if (sourceDAO.update(source)) {

            Source s = identityMap.get(source.getId());

            s.setName(source.getName());
            s.setIconName(source.getIconName());

            return true;
        }
        return false;
    }

    @Override
    public boolean delete(Source source) throws SQLException {
        if (sourceDAO.delete(source)) {
            removeFromCollections(source);

            return true;
        }

        return false;
    }

    @Override
    public boolean add(Source source) throws SQLException {
        if (sourceDAO.add(source)) {
            addToCollections(source);

            return true;
        }

        return false;
    }

    private void addToCollections(Source source) {
        identityMap.put(source.getId(), source);

        if (source.doesHaveParent()) {
            Source parent = identityMap.get(source.getParent().getId());
            if (!parent.getChildren().contains(source)) {
                parent.addChild(source);
            }
        } else {
            sourceMap.get(source.getOperationType()).add(source);
            treeList.add(source);
        }
    }

    private void removeFromCollections(Source source) {
        source = identityMap.remove(source.getId());
        if (source == null) {
            return;
        }

        if (source.doesHaveParent()) {// если удаляем дочерний элемент
            source.getParent().removeChild(source);// т.к. у каждого дочернего элемента есть ссылка на родительский - можно быстро удалять элемент из дерева без поиска по всему дереву
        } else {// если удаляем элемент, у которого нет родителей
            sourceMap.get(source.getOperationType()).remove(source);
            treeList.remove(source);
        }
    }

    @Override
    public List<Source> search(String... params) {
        return sourceDAO.search(params);
    }

    @Override
    public List<Source> getList(OperationType operationType) {
        return sourceMap.get(operationType);
    }

    public Map<Long, Source> getIdentityMap() {
        return identityMap;
    }


    public SourceDAO getSourceDAO() {
        return sourceDAO;
    }
}
