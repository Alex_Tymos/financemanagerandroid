package com.example.training.finance.core.storage.dao.interfaces;


import com.example.training.finance.core.storage.exceptions.CurrencyException;
import com.example.training.finance.core.storage.interfaces.Storage;

import java.math.BigDecimal;
import java.util.Currency;

public interface StorageDAO extends CommonDAO<Storage> {

    String CURRENCY_AMOUNT_TABLE = "currency_amount";
    String STORAGE_TABLE = "storage";

    boolean addCurrency(Storage storage, Currency currency, BigDecimal initAmount) throws CurrencyException;

    boolean deleteCurrency(Storage storage, Currency currency) throws CurrencyException;

    boolean updateAmount(Storage storage, Currency currency, BigDecimal amount);

    int getRefCount(Storage storage);
}
