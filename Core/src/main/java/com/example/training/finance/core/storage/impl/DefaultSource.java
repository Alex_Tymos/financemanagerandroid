package com.example.training.finance.core.storage.impl;


import com.example.training.finance.core.storage.abstracts.AbstractTreeNode;
import com.example.training.finance.core.storage.enums.OperationType;
import com.example.training.finance.core.storage.interfaces.Source;

import java.util.List;

public class DefaultSource extends AbstractTreeNode<DefaultSource> implements Source<DefaultSource> {

    private OperationType operationType;

    public DefaultSource() {
    }

    public DefaultSource(long id) {
        super(id);
    }

    public DefaultSource(String name) {
        super(name);
    }

    public DefaultSource(List<DefaultSource> children) {
        super(children);
    }

    public DefaultSource(long id, List<DefaultSource> children, DefaultSource parent, String name) {
        super(id, children, parent, name);
    }

    public DefaultSource(long id, OperationType operationType) {
        super(id);
        this.operationType = operationType;
    }

    public DefaultSource(String name, OperationType operationType) {
        super(name);
        this.operationType = operationType;
    }

    @Override
    public OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(OperationType operationType) {
        if (!doesHaveParent()) {
            this.operationType = operationType;
        }
    }

    @Override
    public void addChild(DefaultSource child) {
        child.setOperationType(operationType);

        super.addChild(child);
    }

    @Override
    public void setParent(DefaultSource parent) {

        operationType = parent.getOperationType();
        super.setParent(parent);
    }
}
