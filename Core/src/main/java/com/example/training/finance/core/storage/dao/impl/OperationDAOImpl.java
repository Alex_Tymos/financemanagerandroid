package com.example.training.finance.core.storage.dao.impl;


import com.example.training.finance.core.storage.abstracts.AbstractOperation;
import com.example.training.finance.core.storage.dao.interfaces.OperationDAO;
import com.example.training.finance.core.storage.dao.interfaces.SourceDAO;
import com.example.training.finance.core.storage.dao.interfaces.StorageDAO;
import com.example.training.finance.core.storage.database.SQLiteConnection;
import com.example.training.finance.core.storage.enums.OperationType;
import com.example.training.finance.core.storage.impl.operations.ConvertOperation;
import com.example.training.finance.core.storage.impl.operations.IncomeOperation;
import com.example.training.finance.core.storage.impl.operations.OutcomeOperation;
import com.example.training.finance.core.storage.impl.operations.TransferOperation;
import com.example.training.finance.core.storage.interfaces.Operation;
import com.example.training.finance.core.storage.interfaces.Source;
import com.example.training.finance.core.storage.interfaces.Storage;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OperationDAOImpl implements OperationDAO {

    private List<Operation> operationList = new ArrayList<>();

    private Map<Long, Source> sourceIdentityMap;
    private Map<Long, Storage> storageIdentityMap;

    public OperationDAOImpl(Map<Long, Source> sourceIdentityMap, Map<Long, Storage> storageIdentityMap) {
        this.sourceIdentityMap = sourceIdentityMap;
        this.storageIdentityMap = storageIdentityMap;
    }

    @Override
    public List<Operation> getAll() {
        operationList.clear();

        try (Statement stmt = SQLiteConnection.getConnection().createStatement();
             ResultSet rs = stmt.executeQuery("select * from " + OPERATION_TABLE)) {

            while (rs.next()) {

                operationList.add(fillOperation(rs));

            }

            return operationList;

        } catch (SQLException e) {
            Logger.getLogger(OperationDAOImpl.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }


    @Override
    public Operation get(long id) {
        try (PreparedStatement stmt = SQLiteConnection.getConnection().prepareStatement("select * from " + OPERATION_TABLE + " where id=?")) {

            stmt.setLong(1, id);

            try (ResultSet rs = stmt.executeQuery();) {

                if (rs.next()) {
                    AbstractOperation operation = fillOperation(rs);
                    return operation;
                }

            }

        } catch (SQLException e) {
            Logger.getLogger(SourceDAOImpl.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }


    private AbstractOperation fillOperation(ResultSet rs) throws SQLException {

        OperationType operationType = OperationType.getType(rs.getInt("type_id"));


        AbstractOperation operation = createOperation(operationType, rs);

        operation.setId(rs.getLong("id"));
        operation.setOperationType(operationType);
        Calendar datetime = Calendar.getInstance();
        datetime.setTimeInMillis(rs.getLong("datetime"));
        operation.setDateTime(datetime);
        operation.setDescription(rs.getString("description"));

        return operation;
    }


    private AbstractOperation createOperation(OperationType operationType, ResultSet rs) throws SQLException {


        switch (operationType) {
            case INCOME: {
                IncomeOperation operation = new IncomeOperation();

                operation.setFromSource(sourceIdentityMap.get(rs.getLong("from_source_id")));
                operation.setFromCurrency(Currency.getInstance(rs.getString("from_currency_code")));
                operation.setFromAmount(new BigDecimal(rs.getDouble("from_amount")));
                operation.setToStorage(storageIdentityMap.get(rs.getLong("to_storage_id")));


                return operation;
            }
            case OUTCOME: {
                OutcomeOperation operation = new OutcomeOperation();

                operation.setFromStorage(storageIdentityMap.get(rs.getLong("from_storage_id")));
                operation.setFromCurrency(Currency.getInstance(rs.getString("from_currency_code")));
                operation.setFromAmount(new BigDecimal(rs.getDouble("from_amount")));
                operation.setToSource(sourceIdentityMap.get(rs.getLong("to_source_id")));


                return operation;
            }

            case TRANSFER: {
                TransferOperation operation = new TransferOperation();

                operation.setFromStorage(storageIdentityMap.get(rs.getLong("from_storage_id")));
                operation.setFromCurrency(Currency.getInstance(rs.getString("from_currency_code")));
                operation.setFromAmount(new BigDecimal(rs.getDouble("from_amount")));
                operation.setToStorage(storageIdentityMap.get(rs.getLong("to_storage_id")));

                return operation;
            }

            case CONVERT: {
                ConvertOperation operation = new ConvertOperation();

                operation.setFromStorage(storageIdentityMap.get(rs.getLong("from_storage_id")));
                operation.setFromCurrency(Currency.getInstance(rs.getString("from_currency_code")));
                operation.setFromAmount(new BigDecimal(rs.getDouble("from_amount")));


                operation.setToStorage(storageIdentityMap.get(rs.getLong("to_storage_id")));
                operation.setToCurrency((Currency.getInstance(rs.getString("to_currency_code"))));
                operation.setToAmount(new BigDecimal(rs.getDouble("to_amount")));

                return operation;
            }
        }

        return null;
    }

    @Override
    public boolean update(Operation operation) throws SQLException {
        return (delete(operation) && add(operation));
    }

    @Override
    public boolean delete(Operation operation) throws SQLException {
        // TODO реализовать - если есть ли операции по данному хранилищу - запрещать удаление
        try (PreparedStatement stmt = SQLiteConnection.getConnection().prepareStatement("delete from " + OPERATION_TABLE + " where id=?")) {

            stmt.setLong(1, operation.getId());

            if (stmt.executeUpdate() == 1) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean add(Operation operation) {


        String sql = createInsertSql(operation);


        try (PreparedStatement stmt = SQLiteConnection.getConnection().prepareStatement(sql);
             Statement stmtId = SQLiteConnection.getConnection().createStatement()) {

            stmt.setLong(1, operation.getDateTime().getTimeInMillis());
            stmt.setLong(2, operation.getOperationType().getId());
            stmt.setString(3, operation.getDescription());

            switch (operation.getOperationType()) {
                case INCOME:
                    IncomeOperation incomeOperation = (IncomeOperation) operation;

                    stmt.setLong(4, incomeOperation.getFromSource().getId());
                    stmt.setString(5, incomeOperation.getFromCurrency().getCurrencyCode());
                    stmt.setDouble(6, incomeOperation.getFromAmount().doubleValue());
                    stmt.setLong(7, incomeOperation.getToStorage().getId());
                    break;

                case OUTCOME:
                    OutcomeOperation outcomeOperation = (OutcomeOperation) operation;

                    stmt.setLong(4, outcomeOperation.getFromStorage().getId());
                    stmt.setString(5, outcomeOperation.getFromCurrency().getCurrencyCode());
                    stmt.setDouble(6, outcomeOperation.getFromAmount().doubleValue());
                    stmt.setLong(7, outcomeOperation.getToSource().getId());
                    break;

                case TRANSFER:
                    TransferOperation transferOperation = (TransferOperation) operation;

                    stmt.setLong(4, transferOperation.getFromStorage().getId());
                    stmt.setString(5, transferOperation.getFromCurrency().getCurrencyCode());
                    stmt.setDouble(6, transferOperation.getFromAmount().doubleValue());
                    stmt.setLong(7, transferOperation.getToStorage().getId());
                    break;

                case CONVERT:
                    ConvertOperation convertOperation = (ConvertOperation) operation;

                    stmt.setLong(4, convertOperation.getFromStorage().getId());
                    stmt.setString(5, convertOperation.getFromCurrency().getCurrencyCode());
                    stmt.setDouble(6, convertOperation.getFromAmount().doubleValue());
                    stmt.setLong(7, convertOperation.getToStorage().getId());
                    stmt.setString(8, convertOperation.getToCurrency().getCurrencyCode());
                    stmt.setDouble(9, convertOperation.getToAmount().doubleValue());
                    break;
            }

            if (stmt.executeUpdate() == 1) {

                try (ResultSet rs = stmtId.executeQuery("SELECT last_insert_rowid()")) {

                    if (rs.next()) {
                        operation.setId(rs.getLong(1));
                        return true;
                    }
                }
            }

        } catch (SQLException e) {
            Logger.getLogger(SourceDAOImpl.class.getName()).log(Level.SEVERE, null, e);
        }

        return false;
    }

    private String createInsertSql(Operation operation) {

        StringBuilder sb = new StringBuilder("insert into " + OPERATION_TABLE + " (datetime, type_id, description, ");

        switch (operation.getOperationType()) {
            case INCOME:
                return sb.append("from_source_id, from_currency_code, from_amount, to_storage_id) values(?,?,?,?,?,?,?)").toString();
            case OUTCOME:
                return sb.append("from_storage_id, from_currency_code, from_amount, to_source_id) values(?,?,?,?,?,?,?)").toString();
            case TRANSFER:
                return sb.append("from_storage_id, from_currency_code, from_amount, to_storage_id) values(?,?,?,?,?,?,?)").toString();
            case CONVERT:
                return sb.append("from_storage_id, from_currency_code, from_amount, to_storage_id, to_currency_code, to_amount) values(?,?,?,?,?,?,?,?,?)").toString();
        }

        return null;
    }

    @Override
    public List<Operation> getList(OperationType operationType) {
        operationList.clear();

        try (PreparedStatement stmt = SQLiteConnection.getConnection().prepareStatement("select * from " + OPERATION_TABLE + " where type_id=?")) {

            stmt.setLong(1, operationType.getId());

            try (ResultSet rs = stmt.executeQuery()) {

                while (rs.next()) {

                    operationList.add(fillOperation(rs));

                }

                return operationList;

            } catch (SQLException e) {
                Logger.getLogger(OperationDAOImpl.class.getName()).log(Level.SEVERE, null, e);
            }


        } catch (SQLException e) {
            Logger.getLogger(OperationDAOImpl.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

    @Override
    public List<Operation> search(String... params) {
        ArrayList<Operation> list = new ArrayList<>();

        try (PreparedStatement stmt = SQLiteConnection.getConnection().prepareStatement("select * from " + OPERATION_TABLE +
                " where description like ? " +
                "or from_source_id=(select id from " + SourceDAO.SOURCE_TABLE + " where name like ?)" +
                "or to_source_id=(select id from " + SourceDAO.SOURCE_TABLE + " where name like ?)" +
                "or from_storage_id=(select id from " + StorageDAO.STORAGE_TABLE + " where name like?) " +
                "or to_storage_id=(select id from " + StorageDAO.STORAGE_TABLE + " where name like ?)")) {

            String searchStr = "%" + params[0] + "%";

            stmt.setString(1, searchStr);
            stmt.setString(2, searchStr);
            stmt.setString(3, searchStr);
            stmt.setString(4, searchStr);
            stmt.setString(5, searchStr);

            try (ResultSet rs = stmt.executeQuery();) {

                while (rs.next()) {

                    list.add(fillOperation(rs));

                }

                return list;

            } catch (SQLException e) {
                Logger.getLogger(OperationDAOImpl.class.getName()).log(Level.SEVERE, null, e);
            }


        } catch (SQLException e) {
            Logger.getLogger(OperationDAOImpl.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }
}
