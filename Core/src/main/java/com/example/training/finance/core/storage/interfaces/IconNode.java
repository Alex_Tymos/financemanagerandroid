package com.example.training.finance.core.storage.interfaces;


import java.io.Serializable;

public interface IconNode extends Serializable {
    String getName();

    void setName(String name);

    String getIconName();

    void setIconName(String name);

    long getId(); // каждый элемент дерева должен иметь свой уникальный идентификатор

    void setId(long id); // установить id
}

