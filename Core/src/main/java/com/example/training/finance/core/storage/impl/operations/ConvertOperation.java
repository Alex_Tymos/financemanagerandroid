package com.example.training.finance.core.storage.impl.operations;


import com.example.training.finance.core.storage.abstracts.AbstractOperation;
import com.example.training.finance.core.storage.enums.OperationType;
import com.example.training.finance.core.storage.interfaces.Storage;

import java.math.BigDecimal;
import java.util.Currency;

public class ConvertOperation extends AbstractOperation {

    private Storage fromStorage;
    private Storage toStorage;
    private BigDecimal fromAmount;
    private BigDecimal toAmount;
    private Currency fromCurrency;
    private Currency toCurrency;

    public ConvertOperation() {
        super(OperationType.CONVERT);
    }

    public Storage getFromStorage() {
        return fromStorage;
    }

    public void setFromStorage(Storage fromStorage) {
        this.fromStorage = fromStorage;
    }

    public Storage getToStorage() {
        return toStorage;
    }

    public void setToStorage(Storage toStorage) {
        this.toStorage = toStorage;
    }

    public BigDecimal getFromAmount() {
        return fromAmount;
    }

    public void setFromAmount(BigDecimal fromAmount) {
        this.fromAmount = fromAmount;
    }

    public BigDecimal getToAmount() {
        return toAmount;
    }

    public void setToAmount(BigDecimal toAmount) {
        this.toAmount = toAmount;
    }

    public Currency getFromCurrency() {
        return fromCurrency;
    }

    public void setFromCurrency(Currency fromCurrency) {
        this.fromCurrency = fromCurrency;
    }

    public Currency getToCurrency() {
        return toCurrency;
    }

    public void setToCurrency(Currency toCurrency) {
        this.toCurrency = toCurrency;
    }
}
